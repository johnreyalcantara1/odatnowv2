$(document).ready(function () {
    $('#codeFormSubmit').click(function (e) {
        e.preventDefault()
        let id = $('#id').val();
        let bug = $('#bug').is(':checked') ? "on" : '';
        let new_feature = $('#new_feature').is(':checked') ? "on" : '';
        let question = $('#question').is(':checked') ? "on" : '';
        let other = $('#other').is(':checked') ? "on" : '';
        let comments = $('#code_comments').val();

        let data = {
            id,
            bug,
            new_feature,
            question,
            other,
            comments,
        }
        $.ajax({
            url: "/products/optimization/sendto/save",
            headers: {
                'X-CSRF-TOKEN': $('input[name="csrf-token"]').val(),
            },
            type: "POST",
            dataType: 'JSON',
            data: data,
        }).then(response => {
            if (response.success === 200) {
                $('#codeModal').modal('hide')
                $('#codeForm').resetForm()
                $('.alert.alert-success').text(`${response.message}`).show().fadeOut(3000)
            } else {
                $('#codeModal').modal('hide')
                $('.alert.alert-danger').text(`${response.message}`).show().fadeOut(3000)
            }
        })
    })

});
