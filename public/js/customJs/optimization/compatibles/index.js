
$(document).ready(function () {
    $('.alert.alert-success').hide()
    $('.alert.alert-danger').hide()
    let apn1 = $('#apn1')
    let apn2 = $('#apn2')
    let paste = $('#pasteCompatibles')
    let pasteSingle = $('#pasteSingleItem')
    let search_url = $('#search_url')
    let id = $('#product_id').val();
    let data = [];

    if ($('#compatiblesID').val() !== '') {
        let compatiblesID = $('#compatiblesID').val()
        if (compatiblesID) {
            getProductsCompatiblesData(id, compatiblesID)
        }
    } else {
        $('#compatiblesData').html(`
            <tr>
                <td colspan="6" class="text-center text-danger h4">No data available</td>
            </tr>
        `)
    }

    let click = 0
    $('#plusApn').click(function (e) {
        if (click === 0) {
            $('#apnDiv2').show()
            click = 1
        }
        $(this).hide()
    })
    apn1.focusout(function () {
        let data = {
            id,
            apn1: apn1.val()
        }
        if (apn1.val() !== '') {
            saveProductCompatibles(data)
        }
    })
    apn2.focusout(function () {
        let data = {
            id,
            apn2: apn2.val()
        }
        if (apn2.val() !== '' ) {
            saveProductCompatibles(data)
        }
    })
    search_url.focusout(function () {
        let data = {
            id,
            search_url: search_url.val()
        }
        if (search_url.val() !== ''){
            saveProductCompatibles(data)
        }
    })

    paste.focusout(function () {
        let text = $('#pasteCompatibles').val()
        text = text.split(' ')
        text = text.map(item => item.replace(',', ""))
        text = text.map(item => item.trim())
        let unique = text.filter((v, i, a) => a.indexOf(v) === i);
        let data = {
            id,
            pasteCompatibles: paste.val(),
        }
        if (paste.val() !== ''){
            saveProductCompatibles(data, unique)
        }
    })
    $('#modalOpen').click(function () {
        $('#exampleModal').modal('show')
    })
    $('#clickSingleItem').click(function () {
        let data = {
            id,
            pasteCompatibles: pasteSingle.val()
        }
        let unique = []
        unique.push(pasteSingle.val())
        if (pasteSingle.val() !== ""){
            saveProductCompatibles(data, unique)
            $('#exampleModal').modal('hide')
            pasteSingle.val('')
        }
    })


    function saveProductCompatibles(data, uniqueObject=null) {
        $.ajax({
            url: '/products/optimization/compatibles/save',
            headers: {
                'X-CSRF-TOKEN': $('input[name="csrf-token"]').val(),
            },
            type: 'POST',
            dataType: "JSON",
            data: data
        })
        .then(response => {
            if (response.success === 200){
                let compatiblesID = response.compatiblesID
                if ($('#compatiblesID').val() === ''){
                    $('#compatiblesID').val(compatiblesID)
                }
                async function savePromiseFunction (uniqueObject, compatiblesID, id) {
                    uniqueObject.map(model => {
                        saveProductCompatiblesTable(model, compatiblesID, id )
                    })
                    getProductsCompatiblesData(id, compatiblesID, null)
                }
                if (uniqueObject !== null) {
                    savePromiseFunction(uniqueObject, compatiblesID, id)
                }
                $('.alert.alert-success').text(`Done, Please wait...`).show().fadeOut(5000)
            } else if (response.success === 500) {
                $('.alert.alert-success').text(`Done, Please wait...`).show().fadeOut(5000)
            }
        }).catch(response => console.log('catch response', response))
    }
    function saveProductCompatiblesTable(model, compatiblesID, id){
        $.ajax({
            url: `/products/optimization/compatibles/process/${id}/${compatiblesID}`,
            headers: {
                'X-CSRF-TOKEN': $('input[name="csrf-token"]').val(),
            },
            type: 'POST',
            dataType: "JSON",
            data: { model: model }
        })
        .then(response => {
            if (response.success === 200){
                console.log('success', response)
            } else {
                console.log('error', response)
            }
        }).catch(response => console.log('catch response', response))
    }
    function getProductsCompatiblesData(id, compatiblesID, search = null) {
        console.log('id', id)
        console.log('compatiblesID', compatiblesID)
        $.ajax({
            url: `/products/optimization/compatibles/data/${id}/${compatiblesID}`,
            headers: {
                'X-CSRF-TOKEN': $('input[name="csrf-token"]').val(),
            },
            type: 'GET',
            dataType: "JSON",
        })
        .then(response => {
            let data = []
            $('#compatiblesData').html('')
            if (response.length < 1) {
                let row = `
                    <tr>
                        <td colspan="6" class="text-center text-danger h4">No data available</td>
                    </tr>
                `
                $('#compatiblesData').append(row)
            }
            if (search == null || search == '') {
                data = response
                $.each(data, function (key, value) {
                    let color = ''
                    if (value.status === 'review') {
                        color = 'text-danger'
                    } else {
                        color = 'text-success'
                    }
                    let row = `
                            <tr class="dataRow">
                                <td contentEditable="false" data-apn-id="${value.id}" class="apnValue">${value.apn}</td>
                                <td>
                                    <i class="modelStatus cursor-pointer fas fa-clipboard-check cursor-pointer font-size-15 ${color}"
                                       data-status="${value.status}"
                                       data-apn-id="${value.id}"
                                       data-toggle="popover"
                                       title="If approved?"
                                       data-content="Can't set back to review! You can still remove it by deleting it"
                                       data-placement="top"
                                    ></i>
                                </td>
                                <td>
                                    <a href="/products/optimization/compatibles/manuals/list/${id}/${compatiblesID}">
                                        <i class="fas fa-external-link-alt font-size-15 text-primary"></i>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <i class="deleteModel cursor-pointer far fa-trash-alt text-danger font-size-15" data-apn-id="${value.id}"></i>
                                </td>
                            </tr>
                        `
                    $('#compatiblesData').append(row)
                    $('#total').html(data.length)
                    $('.modelStatus').popover('hide')
                })
            }
            if (search !== null ) {
                data = response.filter(dat => dat.apn.toLowerCase().includes(search.toLowerCase()))
                if (data.length > 0) {
                    $.each(data, function (key, value) {
                        let color = ''
                        if (value.status === 'review') {
                            color = 'text-danger'
                        } else {
                            color = 'text-success'
                        }
                        let row = `
                            <tr class="dataRow">
                                <td contentEditable="false" data-apn-id="${value.id}" class="apnValue">${value.apn}</td>
                                <td>
                                    <i
                                    class="modelStatus cursor-pointer fas fa-clipboard-check cursor-pointer font-size-15 ${color}"
                                       data-status="${value.status}"
                                       data-apn-id="${value.id}"
                                       data-toggle="popover"
                                       title="If approved?"
                                       data-content="Can't set back to review! You can still remove it by deleting it"
                                       data-placement="top"
                                    ></i>
                                </td>
                                <td>
                                    <a href="/products/optimization/compatibles/manuals/list/${id}/${compatiblesID}">
                                        <i class="fas fa-external-link-alt font-size-15 text-primary"></i>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <i class="deleteModel cursor-pointer far fa-trash-alt text-danger font-size-15" data-apn-id="${value.id}"></i>
                                </td>
                            </tr>
                        `
                        $('#compatiblesData').append(row)
                        $('#total').html(data.length)
                        $('.modelStatus').popover('hide')

                    })
                } else {
                    let row = `
                    <tr>
                        <td colspan="6" class="text-center text-danger h4">No data available</td>
                    </tr>
                `
                    $('#compatiblesData').append(row)
                    $('#total').html('0')
                }
            }
        }).catch(response => console.log('catch response', response))
    }
    $(document).on('mouseover', '.modelStatus', function () {
        $(this).popover('show')
        console.log($(this))
    })
    $(document).on('mouseleave', '.modelStatus', function () {
        $('.modelStatus').popover('hide')

    })
    //search models
    $('#searchModel').keyup(function (e) {
        $('#clear').show()
        let compatiblesID = $('#compatiblesID').val()
        let search = $('#searchModel').val()
        getProductsCompatiblesData(id, compatiblesID, search)
    })
        let search = $('#searchModel')
    $('#clear').click(function () {
        search.val('')
        let compatiblesID = $('#compatiblesID').val()
        getProductsCompatiblesData(id, compatiblesID)
        $('#clear').hide()

    })
    //change model value
    $(document).on('click', '.apnValue', function () {
        $(this).attr('contentEditable', true).focus()
        console.log($(this).text())
        let recordID = $(this).attr('data-apn-id')
        let model = $(this).text()
        $(this).keyup(function (e) {
            e.preventDefault()
            if (e.keyCode === 13) {
                if (model !== "" && model !== $(this).text() ){
                    changeModel($(this).text(), recordID)
                }
            }
        })
        $(this).focusout(function () {
            if (model !== "" && model !== $(this).text() ){
                changeModel($(this).text(), recordID)
            }
        })

    })
    function changeModel(model, recordID) {
        $.ajax({
            url: `/products/optimization/compatibles/update/${recordID}`,
            data: { model: model },
            type: "POST",
            dataType: 'JSON',
            headers: {
                'X-CSRF-TOKEN': $('input[name="csrf-token"]').val(),
            },
        })
        .then(response => {
            if (response.success === 200) {
                $('.alert.alert-success').text('Model is updated').show().fadeOut(4000)
                getProductsCompatiblesData(id, $('#compatiblesID').val())
            }
        })
    }

    //change Status
    function changeStatus(recordID, status) {
        $.ajax({
            url: `/products/optimization/compatibles/update/status/${recordID}`,
            data: {status: status},
            type: "POST",
            dataType: "JSON",
            headers: {
                'X-CSRF-TOKEN': $('input[name="csrf-token"]').val()
            }
        })
            .then(response => {
                if (response.success === 200) {
                    getProductsCompatiblesData(id, $('#compatiblesID').val() )
                    $('.modelStatus').popover('hide')
                }
            })
    }
    $(document).on('click', '.modelStatus', function () {
        let actStatus = $(this).attr('data-status')
        if (actStatus != 'review'){
            return
        }
        let status = 'approved'
        let recordID = $(this).attr('data-apn-id')
        changeStatus(recordID, status)
    })

//    delete model
    $(document).on('click', '.deleteModel', function () {
        let recordID = $(this).attr('data-apn-id')
        Swal.fire({
            title: 'Are you sure?',
            text: "Manuals and notes for this item will be removed!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: `/products/optimization/compatibles/delete/${recordID}`,
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="csrf-token"]').val(),
                    },
                    type: "GET",
                    dataType: 'JSON',
                })
                    .then(res => {
                        if (res.success === 200) {
                            getProductsCompatiblesData(id, $('#compatiblesID').val())
                            Swal.fire(
                                'Deleted!',
                                'Model has been deleted.',
                                'success'
                            )
                        }
                    })
            }

        })
    })

})
