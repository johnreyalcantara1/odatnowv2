
$(document).ready(function () {
    const backColors = ['rgb(221,84,28)', 'rgb(2,147,109)', 'rgba(238,38,126,1)',  'rgba(105,101,167,1)' ];
    const labels = ['Amazon', 'Organic', 'Google', 'Ebay'];
    // charts divs
    const orders = $('#orders');
    const velocity = $('#velocity');

    let end = moment()
    let start = moment().subtract(6, "month")

    let starMonth = start.format('MMMM')
    let endMonth = end.format('MMMM')

    $('#fromRange').html(starMonth)
    $('#toRange').html(endMonth)

    let salesData = []
    let velocityData = []
    for ($i = 0; $i < 4; $i++) {
        salesData.push(Math.floor(Math.random(0, 5) * 10))
        velocityData.push(Math.floor(Math.random(0, 5) * 10))
    }
    changePieChart(start, end, salesData, velocityData)

    let Picker = $('#picker')
    Picker.daterangepicker({
        ranges: {
            '1 Month': [moment().subtract(1, 'month'), moment()],
            '3 Months': [moment().subtract(3, 'month'), moment()],
            '6 Months': [moment().subtract(6, 'month'), moment()],
            '1 Year': [moment().subtract(12, 'month'), moment()],
            'All': [moment().startOf('year'), moment()],
        },
        "alwaysShowCalendars": true,
        "showCustomRangeLabel": true,
        "autoApply": false,
        "opens": "left",
    }, function(start, end, label) {
        console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');

        $('#picker').text(start.format('YY/MM/DD') + ' to ' + end.format('YY/MM/DD')).css({'font-size': '0.8rem'})
        starMonth = start.format('MMMM')
        endMonth = end.format('MMMM')
        $('#fromRange').html(starMonth)
        $('#toRange').html(endMonth)
        // jquery call with values to send to backend will be placed here
        let sales = []
        let velocity = []
        for ($i = 0; $i < 4; $i++) {
            console.log('creating in picker')
            sales.push(Math.floor(Math.random(0, 5) * 10))
            velocity.push(Math.floor(Math.random(0, 5) * 10))
        }
        console.log('sales: ', sales)
        console.log('velocity: ', velocity)
        changePieChart(start, end, sales, velocity)

    });
    Picker.val(`Date Range `)

    function changePieChart(start, end, ordersData, velocityData) {
        let dataVelocity = {
            datasets: [{
                data: velocityData,
                backgroundColor: backColors,
            }],
            labels: labels
        };
        let velocityChart = new Chart(velocity, {
            type: 'pie',
            data: dataVelocity,
            options: {
                responsive: true,
                legend: {
                    display: false,
                },
                plugins: {
                    labels: {
                        fontColor: '#ffffff',
                        showZero: true,
                    }
                },
            }
        });
        let dataOrders = {
            datasets: [{
                data: ordersData,
                backgroundColor: backColors,
            }],
            labels: labels
        };
        let ordersChart = new Chart(orders, {
            type: 'pie',
            data: dataOrders,
            options: {
                responsive: true,
                title: true,
                legend: {
                    display: false,
                },
                plugins: {
                    labels: {
                        showZero: true,
                        fontColor: '#ffffff',
                        render: function (args) {
                            return '$' + args.value;
                        }
                    }
                }
            }
        });
    }

});
