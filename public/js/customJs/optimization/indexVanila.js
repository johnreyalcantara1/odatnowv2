let id = $('#id').val()
const ajax_update = (route) => {
    let mpn = $('#mpn').val()
    let toSend = ''
    if (route == '/products/optimization/ajax/market') {
        toSend = $("input[name='market_status']:checked ").val()
        if($('#ajax_market label').hasClass('btn-success')) {
            $('#ajax_market label').removeClass('btn-success')
        }
    } else if (route == '/products/optimization/ajax/classification') {
        toSend = $("input[name='classification']:checked ").val()
        $('#classificationLabel1').removeClass('btn-success');
        $('#classificationLabel2').removeClass('btn-success');

    } else if (route == '/products/optimization/ajax/device') {
        toSend = $("#device :checked").val()
    }
    let dataToSend = {
        id,
        data: toSend,
        mpn
    }
    console.log('tosend', toSend)
    $.ajax({
        url: route,
        data: dataToSend,
        type: "POST",
        dataType: 'JSON',
        headers: {
            'X-CSRF-TOKEN': $('input[name="csrf-token"]').val(),
            // "Content-Type": "application/x-www-form-urlencoded"
        },
    })

}
