{{-- Extends layout --}}
@extends('layout.default')


{{-- CSS --}}
@section('styles')
    <link rel="stylesheet" href="/css/customCss/style.css">
    <link rel="stylesheet" href="/css/customCss/pages.css">
@endsection

{{-- Content --}}
@section('content')

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label"><a href="{{ route('products.list') }}">Products</a> :: <a href="{{ route('products.optimization', $product->id) }}">{{ $product->brand_name }}</a> :: <a href="{{ route('products.searchlinks.main', $id) }}">Search Links</a> :: <a href="{{ route('products.searchlinks.list', [$id, $category->id]) }}">{{ $category->name }}</a></h3>
            </div>
        </div>

        <div class="card-body">

            @if ($message = session('message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    {{ $message }}
                </div>
            @endif

            @if ($error = session('error'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    {{ $error }}
                </div>
            @endif

            @if ($warning = session('warning'))
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    {{ $warning }}
                </div>
            @endif

            <h3>Add Search Link</h3>
            <form action="{{ route('products.searchlinks.save') }}" method="post">
                <input type="hidden" name="id" value="{{ $id }}">
                <input type="hidden" name="categoryID" value="{{ $category->id }}">
                @csrf
                <input type="hidden" name="id" value="{{ $id }}">
                <div class="row mt-2">
                    <div class="col-8">
                        <label for="title">Title</label>
                        <input type="text" name="title" id="title" class="form-control" required="required">
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-8">
                        <label for="link">Link</label>
                        <input type="text" name="link" id="link" class="form-control" required="required">
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col">
                        <input type="submit" value="Add Search Link" class="btn btn-success btn-lg">&nbsp;
                        <a href="{{ route('products.optimization', $product->id) }}" class="btn btn-warning btn-lg">Cancel</a>
                    </div>
                </div>

            </form>


        </div>

    </div>

@endsection



{{-- Scripts Section --}}
@section('scripts')
    {{-- vendors --}}

@endsection
