{{-- Extends layout --}}
@extends('layout.default')

{{-- CSS --}}
@section('styles')
    <link rel="stylesheet" href="/css/customCss/style.css">
    <link rel="stylesheet" href="/css/customCss/pages.css">
@endsection

{{-- Content --}}
@section('content')
    <div class="row justify-content-center">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label"><a href="{{ route('products.list') }}">Products</a> :: <a href="{{ route('products.optimization', $product->id) }}">{{ $product->brand_name }}</a> :: <a href="{{ route('products.searchlinks.main', $id) }}">Search Links</a> :: {{ $category->name }}</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-8 col-lg-9">
                            <h1 class="font-weight-bold">Search Links :: {{ $category->name }} <a href="{{ route('products.searchlinks.add', [$id, $categoryID]) }}" class="btn btn-primary">Add</a></h1>
                        </div>

                    </div>
                    @if ($message = session('message'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            {{ $message }}
                        </div>
                    @endif

                    @if ($error = session('error'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            {{ $error }}
                        </div>
                    @endif

                    @if ($warning = session('warning'))
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            {{ $warning }}
                        </div>
                    @endif


                    @if ( $data->count() )
                        @foreach ($data as $index => $i)
                            Title: {{ $i->title }}<br>
                            Link: <a href="{{ $i->link }}" target="_blank">{{ $i->link }}</a><br><br>
                            <a
                                href="{{ route('products.searchlinks.delete', [$i->id, $i->product_id, $i->categoryID]) }}"
                                class="btn btn-danger"
                                onclick="return confirm('WARNING: you are about to delete this link. Click OK to continue')">Delete
                            </a>
                            <hr>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Styles Section --}}
@section('styles')

@endsection


{{-- Scripts Section --}}
@section('scripts')
    {{-- vendors --}}


@endsection
