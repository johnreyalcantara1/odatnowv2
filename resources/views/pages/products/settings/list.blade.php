@extends('layout.default')

@section('styles')
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label mb-0">
                            <a href="{{ route('products.list') }}">Products</a> :: Settings</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-sm-6 col-12 mb-5">
                            <a href="{{route('admin.devicetype.list')}}" >
                                <div class="card bg-success hoverShadowScale">
                                    <div class="card-body d-flex flex-row align-items-center justify-content-start">
                                        <i class="font-size-18 fas fa-laptop-code text-white mr-5"></i>
                                        <span class="text-white">Device Types</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-6 col-12 mb-5">
                            <a href="{{route('admin.flags.list')}}">
                                <div class="card bg-warning hoverShadowScale">
                                    <div class="card-body d-flex flex-row align-items-center justify-content-start">
                                        <i class="font-size-18 fas fa-flag text-white mr-5"></i>
                                        <span class="text-white">Flags</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-6 col-12 mb-5">
                            <a href="{{route('admin.searchlinkscategories.list')}}">
                                <div class="card bg-danger hoverShadowScale">
                                    <div class="card-body d-flex flex-row align-items-center justify-content-start">
                                        <i class="fas fa-search text-white mr-5 font-size-18"></i>
                                        <span class="text-white">Search Links</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-6 col-12 mb-5">
                            <a href="{{route('admin.developerqueue.list')}}">
                                <div class="card bg-primary hoverShadowScale">
                                    <div class="card-body d-flex flex-row align-items-center justify-content-start">
                                        <i class="fas fa-th-list text-white mr-5 font-size-18"></i>
                                        <span class="text-white">Developer Queue</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-sm-6 col-12 mb-5">
                            <a href="{{route('admin.stores.list')}}">
                                <div class="card bg-colorAmazon hoverShadowScale">
                                    <div class="card-body d-flex flex-row align-items-center justify-content-start">
                                        <i class="fas fa-store-alt text-white mr-5 font-size-18"></i>
                                        <span class="text-white">Store Management</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-6 col-12 mb-5">
                            <a href="{{route('admin.brands.list')}}">
                                <div class="card bg-colorOrganic hoverShadowScale">
                                    <div class="card-body d-flex flex-row align-items-center justify-content-start">
                                        <i class="fas fa-tags text-white mr-5 font-size-18"></i>
                                        <span class="text-white">Brands Management</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection
