{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Admin : Search Links Categories : New</h3>
            </div>
        </div>

        <div class="card-body">

            @if ($message = session('message'))
                <div class="alert alert-success">{{ $message }}</div>
            @endif
            <form action="{{ route('admin.searchlinkscategories.save') }}" method="POST">
                @csrf
                <div class="row mt-2">
                    <div class="col-8">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" required="required">
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-8">
                        <label for="type">Type</label>
                        <select name="type" id="type" class="form-control" required="required">
                            <option value="">Select</option>
                            <option value="platform">Platform</option>
                            <option value="general">General</option>
                        </select>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-8">
                        <label for="title">Title</label>
                        <input type="text" name="title" id="title" class="form-control" required="required">
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-8">
                        <label for="placement">Placement (Order)</label>
                        <input type="number" name="placement" id="placement" class="form-control" required="required">
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-8">
                        <label for="image">Image URL</label>
                        <input type="text" name="image" id="image" class="form-control" required="required">
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-8">
                        <input type="submit" value="Save" class="btn btn-primary">&nbsp;
                        <a href="{{ route('admin.searchlinkscategories.list') }}" class="btn btn-warning">Cancel</a>
                    </div>
                </div>
            </form>

        </div>

    </div>

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection


{{-- Scripts Section --}}
@section('scripts')
    {{-- vendors --}}

@endsection
