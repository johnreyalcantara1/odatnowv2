{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Admin : Roles : New</h3>
            </div>
        </div>

        <div class="card-body">

            @if ($message = session('message'))
                <div class="alert alert-success">{{ $message }}</div>
            @endif
            <form action="{{ route('admin.roles.save') }}" method="POST">
                @csrf
            <div class="row mt-2">
                <div class="col-8">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" class="form-control" required="required">
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-8">
                    <label for="slug">Slug</label>
                    <input type="text" name="slug" id="slug" class="form-control" required="required">
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-8">
                    <input type="submit" value="Save" class="btn btn-primary">&nbsp;
                    <a href="{{ route('admin.roles.list') }}" class="btn btn-warning">Cancel</a>
                </div>
            </div>
            </form>

        </div>

    </div>

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection


{{-- Scripts Section --}}
@section('scripts')
    {{-- vendors --}}

@endsection
