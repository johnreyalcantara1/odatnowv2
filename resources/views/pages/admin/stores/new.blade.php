@extends('layout.default')

@section('styles')
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">Admin : Manage Stores : New</h3>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('admin.stores.save') }}" method="POST">
                    @csrf
                    <div class="row mt-2">
                        <div class="col-8">
                            <div class="form-group">
                                <label for="store_id">Store ID</label>
                                <input type="text"
                                    name="store_id" id="store_id" class="form-control" placeholder="Store ID Number"
                                    required
                                    value="{{@old('store_id')}}">
                                @error('store_id')
                                    <small class="text-danger font-weight-bold">* {{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-8">
                            <div class="form-group">
                                <label for="name">Store Name</label>
                                <input type="text"
                                    name="name"
                                    id="name"
                                    class="form-control"
                                    value="{{@old('name')}}"
                                    required
                                    placeholder="Store Name" >
                                @error('name')
                                    <small class="text-danger font-weight-bold">* {{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-8">
                            <div class="form-group">
                                <label for="url">Store URL</label>
                                <input
                                    type="text"
                                    name="url"
                                    id="url"
                                    class="form-control"
                                    value="{{@old('url')}}"
                                    required
                                    placeholder="Store URL" >
                                @error('url')
                                    <small class="text-danger font-weight-bold">* {{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-8">
                            <div class="form-group">
                                <label for="client_id">Store URLClient ID</label>
                                <input
                                    type="text"
                                    name="client_id"
                                    id="client_id"
                                    class="form-control"
                                    value="{{@old('client_id')}}"
                                    required
                                    placeholder="Store Client ID" >
                                @error('client_id')
                                    <small class="text-danger font-weight-bold">* {{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-8">
                            <div class="form-group">
                                <label for="token">Store Token</label>
                                <input
                                    type="text"
                                    name="token"
                                    id="token"
                                    class="form-control"
                                    value="{{@old('token')}}"
                                    required
                                    placeholder="Store Token" >
                                @error('token')
                                    <small class="text-danger font-weight-bold">* {{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-8">
                            <label >Is Main?</label>
                            <div class="form-group">
                                <label for="is_main_1">Yes
                                    <input id="is_main_1" type="radio" name="is_main" class="" value="1">
                                </label>
                                <label for="is_main_2">No
                                    <input id="is_main_2" type="radio" name="is_main" class="" value="0" checked>
                                </label>
                                @error('is_main')
                                    <small class="text-danger font-weight-bold">* {{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-8">
                            <div class="form-group">
                                <input type="submit" value="Save" class="btn btn-primary">&nbsp;
                                <a href="{{ route('admin.flags.list') }}" class="btn btn-warning">Cancel</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@endsection
