{{-- Extends layout --}}
@extends('layout.default')
{{-- Styles Section --}}
@section('styles')
    <link rel="stylesheet" href="/css/customCss/style.css">
    <link rel="stylesheet" href="/css/customCss/pages.css">
@endsection

{{-- Content --}}
@section('content')
   <div class="row" id="compatiblesSection">
       <div class="col">
           <input type="hidden" id="product_id" value="{{$id}}" name="product_id">
           <input type="hidden" id="data" value="" name="data">
           <input type="hidden" id="compatiblesID" value="{{ $compatiblesID ?? '' }}" name="compatiblesID">
           <input name="csrf-token" type="hidden" value="{{ csrf_token() }}" />
           <div class="card card-custom">
               <div class="card-header flex-wrap border-0 pt-6 pb-0 d-flex justify-content-between">
                   <div class="card-title">
                       <h3 class="card-label"><a href="{{ route('products.list') }}">Products</a> :: <a href="{{ route('products.optimization', $id) }}">{{$product->brand_name}}</a> :: Compatibles</h3>
                   </div>
               </div>
               <div class="card-body">
                   <div class="row">
                       <div class="col-12 col-sm-12 col-md-8 col-lg-9">
                           <h1 class="font-weight-bold">Compatibles</h1>
                       </div>
                   </div>
                   @if ($message = session('message'))
                       <div class="alert alert-primary fade show alert-dismissible" role="alert">
                           <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                           {{ $message }}
                       </div>
                   @endif
                   <div id="compatiblesForm" class="p-5">
                       <div class="row justify-content-start align-items-center">
                           <div class="col-5">
                               <div class="form-group text-center">
                                   <label for="apn1" class="font-weight-bold text-center">Alternate Part Number</label>
                                   <input
                                       type="text"
                                       name="apn1"
                                       id="apn1"
                                       value="{{$apn1}}"
                                       class="form-control form-control-sm border-radius-15" placeholder="apn">
                               </div>
                           </div>
                           <div id="apnDiv2" class="col-5">
                               <div class="form-group text-center">
                                   <label for="apn2" class="font-weight-bold text-center">Alternate Part Number - 2</label>
                                   <input type="text"
                                          name="apn2"
                                          id="apn2"
                                          value="{{$apn2}}"
                                          class="form-control form-control-sm border-radius-15" placeholder="apn2">
                               </div>
                           </div>
                       </div>
                       <h4 class="text-center">Compatibles</h4>
                       <hr>
                       <div class="row">
                           <div class="col">
                               <div class="form-group">
                                   <textarea placeholder="Paste here" name="pasteCompatibles" id="pasteCompatibles" class="w-100 border-radius-10 px-3 py-2" cols="30" rows="8">{{$pasteCompatibles}}</textarea>
                               </div>
                               <div class="form-group">
                                   <label for="search_url">Search Url</label>
                                   <input
                                       type="text"
                                       value="{{$search_url}}"
                                       id="search_url"
                                       name="search_url" class="form-control border-radius-10" placeholder="search url">
                               </div>
                               <div>
                                   <div class="alert alert-success"></div>
                                   <div class="alert alert-danger"></div>
                               </div>
                               <div class="row my-5 justify-content-start align-items-end">
                                   <div class="col-9 col-sm-9 col-md-6 col-lg-4">
                                       <label for="searchModel">Search by model</label>
                                       <div class="position-relative">
                                           <input type="text" class="form-control border-radius-10 deletable" placeholder="search by model" id="searchModel" name="searchModel">
                                           <i id="clear" class="cursor-pointer fas fa-times fa-1x"></i>
                                       </div>
                                   </div>
                                   <div class="col-3">
                                        <p class="" >Total:  <span class="badge badge-success" id="total"></span></p>
                                   </div>
                               </div>
                               <div class="form-group">
                                   <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                       <table class="table table-bordered table-striped border position-relative">
                                           <thead>
                                               <tr>
                                                   <th>
                                                       <i id="modalOpen" class=" cursor-pointer mt-1 font-size-15 fas fa-plus-circle text-success mr-2"
{{--                                                          data-toggle="modal"--}}
{{--                                                          data-target="#exampleModal"--}}
                                                       >

                                                       </i>
                                                       <span class="">Model</span>
                                                   </th>
                                                   <th>Status</th>
                                                   <th>Manual</th>
                                                   <th colspan="2" class="text-center">Actions</th>
                                               </tr>
                                           </thead>
                                           <tbody id="compatiblesData">
                                           </tbody>
                                       </table>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="row">
                           <div class="col">
                               <div class="form-group">
                                   <form method="GET" action="{{route('products.compatibles.generate', $id)}}">
                                       <button id="buildCompatiblesProductBtn" class="btn btn-primary btn-sm" type="submit">
                                           Build Compatibles Products
                                       </button>
                                   </form>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>
{{--  modal  --}}
   <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-dialog-centered">
           <div class="modal-content">
               <div class="modal-header">
                   <h5 class="modal-title" id="exampleModalLabel">Add Single Model</h5>
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                   </button>
               </div>
               <div class="modal-body">
                   <div class="form-group">
                       <label for="pasteSingleItem">Add Model</label>
                       <input
                           type="text"
                           value=""
                           id="pasteSingleItem"
                           name="pasteSingleItem" class="form-control border-radius-10"
                           placeholder="Add single model, type or paste">
                   </div>
                   <div class="form-group text-center">
                       <button id="clickSingleItem" type="button" class="btn btn-secondary rounded-pill btn-sm">Save</button>
                   </div>
               </div>
               <div class="modal-footer">
               </div>
           </div>
       </div>
   </div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="/js/customJs/optimization/compatibles/index.js"></script>
@endsection
