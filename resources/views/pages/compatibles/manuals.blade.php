{{-- Extends layout --}}
@extends('layout.default')
{{-- Styles Section --}}
@section('styles')
    <link rel="stylesheet" href="/css/customCss/style.css">
    <link rel="stylesheet" href="/css/customCss/pages.css">
@endsection
{{--content section bellow--}}
@section('content')
    <div class="row" id="compatiblesManualsSection">
        <div class="col">
            <input name="csrf-token" type="hidden" value="{{ csrf_token() }}" />
            <input name="id" type="hidden" id="id" value="{{ $id }}" />
            <input name="compatiblesID" type="hidden" id="compatiblesID" value="{{ $compatiblesID }}" />
            <div class="card card-custom mb-5">
                <div class="card-header flex-wrap border-0 pt-6 pb-0 d-flex justify-content-between">
                    <div class="card-title">
                        <h3 class="card-label">
                            <a href="{{ route('products.list') }}">Products</a> ::
                            <a href="{{ route('products.optimization', $id) }}">{{$product->brand_name}}</a> ::
                            <a href="{{ route('products.compatibles.list', $id) }}">Compatibles</a> ::
                            Manuals</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-8 col-lg-9">
                            <h1 class="font-weight-bold">Manuals</h1>
                        </div>
                    </div>
                    <hr>
                    <div class="row justify-content-between">
                        <div class="col-12 col-md-6 col-lg-6">
                            <h4>List</h4>
                            <hr>
                            <div class="table-wrapper-scroll-y my-custom-scrollbar-350">
                                <table class="table table-bordered table-striped">
                                    <thead class="bg-secondary">
                                        <tr>
                                            <th>#</th>
                                            <th>Manual Type</th>
                                            <th>link</th>
                                            <th>Note</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="manualsList">
                                        @foreach($data as $key => $manual)
                                            <tr>
                                                <td>{{$key + 1}}</td>
                                                <td>{{$manual->manual_type}}</td>
                                                <td>
                                                    <a target="_blank" href="{{$manual->link}}">
                                                        <i class="fas fa-external-link-alt font-size-15 text-primary"></i>
                                                    </a>
                                                </td>
                                                <td>{{$manual->note}}</td>
                                                <td class="text-center">
                                                    <i class="deleteManual cursor-pointer far fa-trash-alt text-danger font-size-15"
                                                       data-manual-id="{{$manual->id}}"></i>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-6">
                            <h4>Add new Manual</h4>
                            <hr>
                            <div id="newManualForm">
                                <div class="form-group">
                                    <label for="link">Manual Link *</label>
                                    <input class="form-control" id="link" name="link" type="text" placeholder="manual link">
                                    <div class="alert alert-danger link"></div>
                                </div>
                                <div class="form-group">
                                    <label for="note">Manual Note</label>
                                    <textarea class="form-control" cols="10" rows="5" id="note" name="note" type="text" placeholder="manual note"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="manual_id">Manual Type *</label>
                                    <select id="manual_id" class="form-control" name="manual_id">
                                        <option style="display: none" value="">Choose Type</option>
                                        @foreach($types as $type)
                                            <option value="{{$type->id}}">{{$type->manual_type}}</option>
                                        @endforeach
                                    </select>
                                    <div class="alert alert-danger type"></div>
                                </div>
                                <div class="form-group">
                                    <button type="button" id="createManual" class="btn btn-primary">Create</button>
                                </div>
                                <div class="form-group">
                                    <div class="alert alert-success"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="/js/customJs/optimization/compatibles/manuals.js"></script>
@endsection
