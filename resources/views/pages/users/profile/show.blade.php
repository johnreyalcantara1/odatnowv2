{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Edit Profile</h3>
            </div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group row">
                        <label for="name" class="col-md-3 col-form-label">Name</label>
                        <div class="col-md-9">
                            <p class="form-control-plaintext text-muted">{{ $user->name }}</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-3 col-form-label">Email</label>
                        <div class="col-md-9">
                            <p class="form-control-plaintext text-muted">{{ $user->email }}</p>
                        </div>
                    </div>
                </div>
            </div>

            <hr>
            <div class="form-group row mb-0">
                <div class="col-md-9 offset-md-3">
                    <a href="{{ route('profile.edit') }}" class="btn btn-info">Edit</a>
                    <a href="{{ route('dashboard') }}" class="btn btn-warning">Cancel</a>
                </div>
            </div>

        </div>
    </div>
@endsection
