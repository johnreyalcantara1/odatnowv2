{{-- Extends layout --}}
@extends('layout.default')

{{-- CSS --}}
@section('styles')
    <link rel="stylesheet" href="/css/customCss/style.css">
    <link rel="stylesheet" href="/css/customCss/pages.css">
@endsection

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label"><a href="{{ route('products.list') }}">Products</a> :: <a href="{{ route('products.optimization', $product->id) }}">{{ $product->brand_name }}</a> :: Notifications</h3>
                </div>
            </div>
            <div class="card-body">
                @if ($message = session('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        {{ $message }}
                    </div>
                @endif

                @if ($error = session('error'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        {{ $error }}
                    </div>
                @endif

                @if ($warning = session('warning'))
                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        {{ $warning }}
                    </div>
                @endif
                <a href="{{ route('products.notifications.add', $id) }}" class="btn btn-primary btn-lg">Add Notification</a>
                <table class="table table-bordered table-hover mt-2">
                    <thead>
                    <tr>
                        <th>Product ID</th>
                        <th>SKU</th>
                        <th>User</th>
                        <th>Reason</th>
                        <th>Notes</th>
                        <th>Date Added</th>
                        <th>Date Updated</th>
                        <th>Dismiss</th>
                        <th>Snooze</th>

                    </tr>
                    </thead>
                    <tbody>

                    @if ($data->count())
                        @foreach ($data as $index => $i)
                            <tr>
                                <td>{{ $i->product_id }}</td>
                                <td>{{ $i->sku }}</td>
                                <td>{{ $i->name }}</td>
                                <td>{{ $i->reason }}</td>
                                <td>{{ $i->notes }}</td>
                                <td>{{ $i->date_added }}</td>
                                <td>{{ $i->date_updated }}</td>
                                <td>
                                    <form name="MyForm_{{ $i->id }}" method="POST">
                                    @csrf
                                    <label class="switch">
                                        <input
                                            type="checkbox"
                                            name="dismiss"
                                            value="on"
                                            @if($i->dismiss == "on")
                                                checked
                                            @endif
                                            onclick="ajax_update(this.form, '{{ route('products.notifications.dismiss', $i->id) }}', '#null')"
                                        >
                                        <span class="slider round"></span>
                                    </label>
                                    </form>
                                </td>
                                <td>
                                    <form name="MyForm2_{{ $i->id }}" method="POST">
                                    @csrf
                                    <label class="switch">
                                        <input
                                            type="checkbox"
                                            name="snooze"
                                            value="on"
                                            @if($i->snooze == "on")
                                            checked
                                            @endif
                                            onclick="ajax_update(this.form, '{{ route('products.notifications.snooze', $i->id) }}', '#null')"
                                        >
                                        <span class="slider round"></span>
                                    </label>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                </table>
                {{ $data->appends(request()->only('_id'))->links() }}
            </div>

        </div>
    </div>
</div>

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection


{{-- Scripts Section --}}
@section('scripts')
    {{-- vendors --}}

    <script>
    function ajax_update(myForm, myRoute, id) {
        $.post(myRoute,
        $(myForm).serialize(),
        function(php_msg) {
        {{-- id would be the div element. IE #mydiv --}}
            $(id).html(php_msg);
        });
    }
    </script>

@endsection
