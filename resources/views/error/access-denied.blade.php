{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <main class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-title">
                            <div class="d-flex align-items-center">
                                <h2 class="mb-0">Error</h2>

                            </div>
                        </div>
                        <div class="card-body">

                            You do not have access to the page requested.

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
