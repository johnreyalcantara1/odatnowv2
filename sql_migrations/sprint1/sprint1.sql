ALTER TABLE `products` ADD `classification` VARCHAR(50) NULL AFTER `battery_cover`;
ALTER TABLE `mpn_market_status` ADD `productID` INT NULL COMMENT 'products.id' AFTER `status`;

CREATE TABLE `device_type` ( `id` BIGINT(20) NOT NULL AUTO_INCREMENT , `device` VARCHAR(255) NOT NULL , `created_at` TIMESTAMP NOT NULL , `updated_at` TIMESTAMP NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `products` ADD `device` VARCHAR(255) NULL AFTER `classification`;

CREATE TABLE `notifications` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `product_id` INT NOT NULL , `sku` VARCHAR(255) NULL , `user` INT NULL , `reason` TEXT NULL , `notes` TEXT NULL , `dismiss` VARCHAR(20) NULL , `snooze` VARCHAR(20) NULL , `date_updated` TIMESTAMP NULL , `date_added` TIMESTAMP NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
