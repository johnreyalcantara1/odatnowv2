<?php
// Header menu
return [

    'items' => [
        [],
        [
            'title' => 'Dashboard',
            'root' => true,
            'page' => '/',
            'new-tab' => false,
        ],
        [
            'title' => 'Logout',
            'root' => true,
            'page' => '/logout',
            'new-tab' => false,
        ],
    ]

];
