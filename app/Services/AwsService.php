<?php

namespace App\Services;
use Aws\Sdk;

class AwsService
{
    private $aws;

    public function __construct(Sdk $aws)
    {
        $this->aws = $aws;
    }

    /**
     * @param string $bucket
     * @param string $path
     * @param string $name
     * @return string
     */
    public function putObject(string $bucket, string $path, string $name): string
    {
        $aws = $this->aws;

        try {
            $client = $aws->createS3();
            $result = $client->putObject([
                'Bucket' => $bucket,
                'Key' => $name,
                'SourceFile' => $path
            ]);
            $client->waitUntil('ObjectExists', ['Bucket' => $bucket, 'Key' => $name]);

            return $result['ObjectURL'];
        } catch (\Exception $e) {
            $myresponse = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            header('Content-Type: application/json');
            print json_encode($myresponse);
            die;
        }
    }

    /**
     * @param string $bucket
     * @param string $key
     * @return \Aws\Result
     */
    public function getObject(string $bucket, string $key)
    {
        $aws = $this->aws;

        try {
            $client = $aws->createS3();
            $result = $client->getObject([
                'Bucket' => $bucket,
                'Key' => $key
            ]);

            return $result;
        } catch (\Exception $e) {
            //return 'error: ' . $e->getMessage();
            $myresponse = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
            header('Content-Type: application/json');
            print json_encode($myresponse);
            die;
        }
    }
}
