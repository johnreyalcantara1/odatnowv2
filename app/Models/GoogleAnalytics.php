<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoogleAnalytics extends Model
{
    protected $table = 'google_analytics';
    protected $fillable = ['page_views', 'sessions', 'users'];


}
