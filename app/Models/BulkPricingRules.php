<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BulkPricingRules extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bulk_pricing_rules';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'product_id',
                  'quantity_min',
                  'quantity_max',
                  'type',
                  'amount',
                  'bulk_id'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the Product for this model.
     *
     * @return App\Models\Product
     */
    public function Product()
    {
        return $this->belongsTo('App\Models\Product','product_id','id');
    }

    /**
     * Get the bulk for this model.
     *
     * @return App\Models\Bulk
     */
    public function bulk()
    {
        return $this->belongsTo('App\Models\Bulk','bulk_id');
    }

    /**
     * @param $product
     * @param $rules
     * @return bool
     */
    public static function insertBulkPricingRules($product, $rules)
    {
        for($i = 0; $i < count($rules); $i++) {
            $bulk = new BulkPricingRules();
            $bulk->product_id = $product->id;
            $bulk->quantity_min = $rules[$i]['quantity_min'];
            $bulk->quantity_max = $rules[$i]['quantity_max'];
            $bulk->type = $rules[$i]['type'];
            $bulk->amount = $rules[$i]['amount'];
            $bulk->bulk_id = $rules[$i]['id'];
            $bulk->save();
        }
        return true;
    }

}
