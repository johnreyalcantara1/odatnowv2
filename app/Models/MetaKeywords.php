<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetaKeywords extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'meta_keywords';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'product_id',
                  'meta_keywords'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the Product for this model.
     *
     * @return App\Models\Product
     */
    public function Product()
    {
        return $this->belongsTo('App\Models\Product','product_id','id');
    }

    /**
     * @param $product
     * @param $keywords
     * @return bool
     */
    public static function insertMetaKeywords($product, $keywords)
    {
        for($i = 0; $i < count($keywords); $i++){
            $meta = new MetaKeywords();
            $meta->product_id = $product->id;
            $meta->meta_keywords = $keywords[$i];
            $meta->save();
        }
        return true;
    }

}
