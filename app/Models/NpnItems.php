<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NpnItems extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'npn_items';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'load_id',
                  'npn_box_id',
                  'brand',
                  'npn_sku',
                  'image_name',
                  'image_url_1',
                  'image_url_2',
                  'image_url_3',
                  'notes',
                  'user',
                  'date_created'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the Load for this model.
     *
     * @return App\Models\Load
     */
    public function Load()
    {
        return $this->belongsTo('App\Models\Load','load_id','id');
    }

    /**
     * Get the NpnBox for this model.
     *
     * @return App\Models\NpnBox
     */
    public function NpnBox()
    {
        return $this->belongsTo('App\Models\NpnBox','npn_box_id','id');
    }

    /**
     * Get the User for this model.
     *
     * @return App\Models\User
     */
    public function User()
    {
        return $this->belongsTo('App\Models\User','user','id');
    }



}
