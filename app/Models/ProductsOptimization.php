<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsOptimization extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products_optimization';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'index_id',
                  'product_id',
                  'name',
                  'type',
                  'sku',
                  'mpn',
                  'description',
                  'weight',
                  'width',
                  'depth',
                  'height',
                  'price',
                  'cost_price',
                  'retail_price',
                  'sale_price',
                  'tax_class_id',
                  'product_tax_code',
                  'brand_id',
                  'brand_name',
                  'inventory_level',
                  'inventory_level_new',
                  'inventory_level_used',
                  'inventory_warning_level',
                  'inventory_tracking',
                  'fixed_cost_shipping_price',
                  'is_free_shipping',
                  'is_visible',
                  'is_featured',
                  'warranty',
                  'bin_picking_number',
                  'layout_file',
                  'upc',
                  'search_keywords',
                  'availability',
                  'availability_description',
                  'gift_wrapping_options_type',
                  'sort_order',
                  'product_condition',
                  'is_condition_shown',
                  'order_quantity_minimum',
                  'order_quantity_maximum',
                  'page_title',
                  'meta_description',
                  'view_count',
                  'preorder_release_date',
                  'preorder_message',
                  'is_preorder_only',
                  'is_price_hidden',
                  'price_hidden_label',
                  'calculated_price',
                  'date_created',
                  'date_modified',
                  'adwords_on',
                  'google_shopping_on',
                  'is_compatible',
                  'date_updated',
                  'date_updated_unix',
                  'user_id',
                  'updated_by',
                  'marketability_score',
                  'status',
                  'ebay_eligible',
                  'amazon_eligible',
                  'google_eligible',
                  'is_watched',
                  'is_purchased',
                  'is_data_error'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the index for this model.
     *
     * @return App\Models\Index
     */
    public function index()
    {
        return $this->belongsTo('App\Models\Index','index_id');
    }

    /**
     * Get the product for this model.
     *
     * @return App\Models\Product
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }

    /**
     * Get the taxClass for this model.
     *
     * @return App\Models\TaxClass
     */
    public function taxClass()
    {
        return $this->belongsTo('App\Models\TaxClass','tax_class_id');
    }

    /**
     * Get the brand for this model.
     *
     * @return App\Models\Brand
     */
    public function brand()
    {
        return $this->belongsTo('App\Models\Brand','brand_id');
    }

    /**
     * Get the user for this model.
     *
     * @return App\Models\User
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    /**
     * Get the updater for this model.
     *
     * @return App\User
     */
    public function updater()
    {
        return $this->belongsTo('App\User','updated_by');
    }



}
