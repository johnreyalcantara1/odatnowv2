<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCompatiblesTable extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products_compatibles_table';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'compatibles_id',
        'apn',
        'status',
        'manual'
    ];

    public static function locateModels($product, $compatiblesID, $model)
    {
        $result = ProductCompatiblesTable::from('products_compatibles_table as c')
            ->select('c.id')
            ->where('c.product_id', $product->id)
            ->where('c.compatibles_id', $compatiblesID)
            ->where('c.apn', $model)
            ->take(1)
            ->get()
        ;
        return $result;
    }

    public static function getTableData($product, $compatiblesID)
    {
        $result = ProductCompatiblesTable::from('products_compatibles_table as c')
            ->select('c.*')
            ->where('c.product_id', $product->id)
            ->where('c.compatibles_id', $compatiblesID)
            ->get()
        ;
        return $result;
    }
}
