<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SearchLinks extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *ß
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'search_links';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'categoryID',
        'title',
        'link',
        'userID'
    ];

    public static function getCategoryLinks($id, $categoryID)
    {
        $data = SearchLinks::from('search_links as s')
            ->select(
                's.id',
                's.product_id',
                's.categoryID',
                's.title',
                's.link'
            )
            ->where('s.product_id', $id)
            ->where('s.categoryID', $categoryID)
            ->get()
        ;
        return $data;
    }

}
