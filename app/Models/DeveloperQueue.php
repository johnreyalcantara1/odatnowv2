<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeveloperQueue extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'developer_queue';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'bug',
        'new_feature',
        'question',
        'other',
        'comments',
        'userID',
        'status'
    ];

    public static function listRecords()
    {
        $data = DeveloperQueue::from('developer_queue as d')
            ->select(
                'd.id',
                'd.product_id',
                'd.bug',
                'd.new_feature',
                'd.question',
                'd.other',
                'd.comments',
                'd.status',
                'd.date_added',
                'u.name',
                'p.sku',
                'p.bin_picking_number',
                'p.brand_name',
                'p.mpn'
            )
            ->leftJoin('users as u', 'u.id', '=', 'd.userID')
            ->leftJoin('products as p', 'p.id', '=', 'd.product_id')
            ->orderBy('d.status', 'DESC')
            ->paginate($_ENV['PAGINATE'])
        ;
        return $data;
    }
}
