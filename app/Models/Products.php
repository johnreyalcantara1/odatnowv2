<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'product_id',
                  'name',
                  'type',
                  'sku',
                  'mpn',
                  'description',
                  'weight',
                  'width',
                  'depth',
                  'height',
                  'price',
                  'cost_price',
                  'retail_price',
                  'sale_price',
                  'tax_class_id',
                  'product_tax_code',
                  'brand_id',
                  'brand_name',
                  'inventory_level',
                  'inventory_level_new',
                  'inventory_level_used',
                  'inventory_warning_level',
                  'inventory_tracking',
                  'fixed_cost_shipping_price',
                  'is_free_shipping',
                  'is_visible',
                  'is_featured',
                  'warranty',
                  'bin_picking_number',
                  'layout_file',
                  'upc',
                  'search_keywords',
                  'availability',
                  'availability_description',
                  'gift_wrapping_options_type',
                  'sort_order',
                  'product_condition',
                  'is_condition_shown',
                  'order_quantity_minimum',
                  'order_quantity_maximum',
                  'page_title',
                  'meta_description',
                  'meta_keywords',
                  'view_count',
                  'preorder_release_date',
                  'preorder_message',
                  'is_preorder_only',
                  'is_price_hidden',
                  'price_hidden_label',
                  'calculated_price',
                  'date_created',
                  'date_modified',
                  'image_age',
                  'variant_id',
                  'adwords_on',
                  'google_shopping_on',
                  'google_shopping_exclude',
                  'is_compatible',
                  'status',
                  'last_inventory',
                  'compatible_status',
                  'adwords_status',
                  'ebay_eligible',
                  'amazon_eligible',
                  'amazon_flag_pricing',
                  'amazon_flag_no_asin',
                  'amazon_flag_restricted',
                  'amazon_flag_needs_support_call',
                  'amazon_flag_other',
                  'amazon_flag_watch_ranking',
                  'marketability_score',
                  'google_eligible',
                  'is_watched',
                  'is_purchased',
                  'is_data_error',
                  'last_updated',
                  'processing_flag',
                  'last_optimized',
                  'product_marketable_status',
                  'last_date_have_inventory',
                  'last_date_sold_out',
                  'is_prime',
                  'battery_cover',
                  'classification',
                  'device',
                  'bluetooth',
                  'wifi',
                  'rf',
                  'infrared',
                  'star_rating'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the product for this model.
     *
     * @return App\Models\Product
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }

    /**
     * Get the taxClass for this model.
     *
     * @return App\Models\TaxClass
     */
    public function taxClass()
    {
        return $this->belongsTo('App\Models\TaxClass','tax_class_id');
    }

    /**
     * Get the brand for this model.
     *
     * @return App\Models\Brand
     */
    public function brand()
    {
        return $this->belongsTo('App\Models\Brand','brand_id');
    }

    /**
     * Get the variant for this model.
     *
     * @return App\Models\Variant
     */
    public function variant()
    {
        return $this->belongsTo('App\Models\Variant','variant_id');
    }

    /**
     * Get the bulkPricingRule for this model.
     *
     * @return App\Models\BulkPricingRule
     */
    public function bulkPricingRule()
    {
        return $this->hasOne('App\Models\BulkPricingRule','product_id','id');
    }

    /**
     * Get the categories for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function categories()
    {
        return $this->hasMany('App\Models\Category','product_id','id');
    }

    /**
     * Get the customFields for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function customFields()
    {
        return $this->hasMany('App\Models\CustomField','product_id','id');
    }

    /**
     * Get the customFields1 for this model.
     *
     * @return App\Models\CustomFields1
     */
    public function customFields1()
    {
        return $this->hasOne('App\Models\CustomFields1','product_id','id');
    }

    /**
     * Get the customUrls for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function customUrls()
    {
        return $this->hasMany('App\Models\CustomUrl','product_id','id');
    }

    /**
     * Get the giftWrappingOptionsList for this model.
     *
     * @return App\Models\GiftWrappingOptionsList
     */
    public function giftWrappingOptionsList()
    {
        return $this->hasOne('App\Models\GiftWrappingOptionsList','product_id','id');
    }

    /**
     * Get the googleShoppingProductList for this model.
     *
     * @return App\Models\GoogleShoppingProductList
     */
    public function googleShoppingProductList()
    {
        return $this->hasOne('App\Models\GoogleShoppingProductList','product_id','id');
    }

    /**
     * Get the imageAudits for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function imageAudits()
    {
        return $this->hasMany('App\Models\ImageAudit','product_id','id');
    }

    /**
     * Get the images for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function images()
    {
        return $this->hasMany('App\Models\Image','product_id','id');
    }

    /**
     * Get the itemNotes for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function itemNotes()
    {
        return $this->hasMany('App\Models\ItemNote','product_id','id');
    }

    /**
     * Get the metaKeywords for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function metaKeywords()
    {
        return $this->hasMany('App\Models\MetaKeyword','product_id','id');
    }

    /**
     * Get the notes for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function notes()
    {
        return $this->hasMany('App\Models\Note','product_id','id');
    }

    /**
     * Get the relatedProduct for this model.
     *
     * @return App\Models\RelatedProduct
     */
    public function relatedProduct()
    {
        return $this->hasOne('App\Models\RelatedProduct','product_id','id');
    }

    /**
     * Get the rtiItems for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function rtiItems()
    {
        return $this->hasMany('App\Models\RtiItem','product_id','id');
    }

    /**
     * Get the soldInventoryHistories for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function soldInventoryHistories()
    {
        return $this->hasMany('App\Models\SoldInventoryHistory','product_id','id');
    }

    /**
     * Get the temporaryImageTable for this model.
     *
     * @return App\Models\TemporaryImageTable
     */
    public function temporaryImageTable()
    {
        return $this->hasOne('App\Models\TemporaryImageTable','product_id','id');
    }

    /**
     * Get the video for this model.
     *
     * @return App\Models\Video
     */
    public function video()
    {
        return $this->hasOne('App\Models\Video','product_id','id');
    }

    public static function productList($search_text)
    {
        $data = Products::from('products as p')
            ->select(
                'p.id',
                'p.sku',
                'p.bin_picking_number',
                'p.brand_name',
                'p.mpn',
                'p.price',
                'p.product_condition',
                'p.inventory_level',
                'p.last_optimized',
                'p.classification',
                'p.device',
                'p.product_id',
                'i.url_thumbnail',
                'i2.url_thumbnail AS url_thumbnail2',
                'c.primary_product_id',
                'c.child_product_id'
            )
            ->leftJoin('compatibles_product_child_table as c', function($child)
            {
                $child->on('c.child_product_id', '=', 'p.id');
            })
            ->leftJoin('images as i', function($leftJoin)
            {
                $leftJoin->on('i.product_id', '=', 'p.id')
                    ->where('i.is_thumbnail', '=', '1');
            })
            ->leftJoin('images as i2', function($leftJoin2)
            {
                $leftJoin2->on('i2.product_id', '=', 'c.primary_product_id')
                    ->where('i2.is_thumbnail', '=', '1');
            })
            ->where(function($query)use ($search_text) {
                //$query->whereNotNull('p.product_id');
                $query->where('p.status', '!=', 'trash');
                if ($search_text != "") {
                    $query->where('p.brand_name', 'like', '%'.$search_text.'%');
                    $query->orWhere('p.mpn', 'like', '%'.$search_text.'%');
                    $query->orWhere('p.sku', 'like', '%'.$search_text.'%');
                }
            })
            ->orderBy('id', 'DESC')
            ->paginate($_ENV['PAGINATE'])
        ;
        return $data;
    }
    public static function optimization($id)
    {
        $data = Products::from('products as p')
            ->select(
                'p.id',
                'p.product_id',
                'p.sku',
                'p.bin_picking_number',
                'p.brand_name',
                'p.mpn',
                'p.price',
                'p.product_condition',
                'p.inventory_level',
                'p.last_optimized',
                'p.classification',
                'p.device',
                'p.status',
                'p.bluetooth',
                'p.wifi',
                'p.rf',
                'p.infrared',
                'p.star_rating',
                'p.location',
                'i.url_thumbnail',
                'i.url_standard',
                'i.url_zoom',
                'i2.url_thumbnail AS url_thumbnail2',
                'i2.url_standard AS url_standard2',
                'i2.url_zoom AS url_zoom2',
                'c.primary_product_id',
                'c.child_product_id'
            )

            ->where('p.id', $id)
            ->leftJoin('compatibles_product_child_table as c', function($child)
            {
                $child->on('c.child_product_id', '=', 'p.id');
            })
            ->leftJoin('images as i', function($leftJoin)
            {
                $leftJoin->on('i.product_id', '=', 'p.id')
                    ->where('i.is_thumbnail', '=', '1');
            })
            ->leftJoin('images as i2', function($leftJoin2)
            {
                $leftJoin2->on('i2.product_id', '=', 'c.primary_product_id')
                    ->where('i2.is_thumbnail', '=', '1');
            })
            ->get()
        ;
        return $data;
    }

    public static function getProductImages($id)
    {
        $data = Products::from('products as p')
            ->select(
                'p.id',
                'i.url_thumbnail',
                'i.url_standard',
                'i.url_zoom'
            )
            ->where('p.id', '=', $id)
            ->leftJoin('images as i', function($leftJoin)
            {
                $leftJoin->on('i.product_id', '=', 'p.id')
                    ->where('i.is_thumbnail', '=', '1');
            })
            ->get();
        ;
    }

    public static function getNextForReview($id)
    {
        $data = Products::from('products as p')
            ->select('p.id')
            ->where('p.status', '=', 'for review')
            ->where('p.id', '>', $id)
            ->take(1)
            ->get();

        return $data;
    }

    public static function getPriorForReview($id)
    {
        $data = Products::from('products as p')
            ->select('p.id')
            ->where('p.status', '=', 'for review')
            ->where('p.id', '<', $id)
            ->take(1)
            ->get();

        return $data;
    }

    public static function checkChildDuplicates($id)
    {
        $data = Products::from('products as p1')
            ->select(
                'p1.id',
                'c.primary_product_id'
            )
            ->where('p1.id', '=', $id)
            ->leftJoin('compatibles_product_child_table as c', function($leftJoin)
            {
                $leftJoin->on('c.primary_product_id', '=', 'p1.id');
            })
            ->get();

        return $data;
    }

    public static function checkIfChild($id)
    {
        $data = Products::from('products as p1')
            ->select(
                'p1.id',
                'c.primary_product_id',
                'c.child_product_id'
            )
            ->where('p1.id', '=', $id)
            ->leftJoin('compatibles_product_child_table as c', function($leftJoin)
            {
                $leftJoin->on('c.child_product_id', '=', 'p1.id');
            })
            ->get()
        ;

        return $data;
    }
}
