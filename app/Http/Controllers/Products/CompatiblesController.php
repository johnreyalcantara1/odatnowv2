<?php

namespace App\Http\Controllers\Products;

use App\Models\Flags;
use App\Models\ProductCompatiblesManuals;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\ProductCompatibles;
use App\Models\ProductCompatiblesTable;
use App\Models\CompatiblesManualTypes;
use App\Models\CompatiblesProductChildTable;

class CompatiblesController extends Controller
{
    public function listAction($id, Request $request)
    {
        $page_title = "Compatibles";

        $product = Products::find($id);
        $query = ProductCompatibles::locateCompatibles($product);
        $compatiblesID = "";
        $pasteCompatibles = "";
        $search_url = "";
        $apn1 = "";
        $apn2 = "";

        foreach ($query as $q) {
            if (is_object($q)) {
                $compatibles = ProductCompatibles::find($q->id);
                $compatiblesID = $q->id;
                $pasteCompatibles = $compatibles->compatibles;
                $search_url = $compatibles->search_url;
                $apn1 = $compatibles->apn1;
                $apn2 = $compatibles->apn2;
            }
        }

        return view('pages.compatibles.list', [
            'page_title' => $page_title,
            'id' => $id,
            'compatiblesID' => $compatiblesID,
            'pasteCompatibles' => $pasteCompatibles,
            'search_url' => $search_url,
            'apn1' => $apn1,
            'apn2' => $apn2,
            'product' => $product,
        ]);
    }

    public function saveAction(Request $request)
    {
        $id = $request->request->get('id');
        $product = Products::find($id);
        $query = ProductCompatibles::locateCompatibles($product);
        $compatibles = "";

        // Add or update primary fields
        $found = 0;
        foreach ($query as $q) {
            $found = 1;
            if (is_object($q)) {
                // Update
                $compatibles = ProductCompatibles::find($q->id);
                if ($request->request->get('pasteCompatibles') != "") {
                    $compatibles->compatibles = $request->request->get('pasteCompatibles');
                }
                if ($request->request->get('search_url') != "") {
                    $compatibles->search_url = $request->request->get('search_url');
                }
                if ($request->request->get('apn1') != "") {
                    $compatibles->apn1 = $request->request->get('apn1');
                }
                if ($request->request->get('apn2') != "") {
                    $compatibles->apn2 = $request->request->get('apn2');
                }
                $compatibles->save();
            }
        }

        if ($found == 0) {
            // new
            $compatibles = new ProductCompatibles();
            $compatibles->product_id = $id;
            if ($request->request->get('pasteCompatibles') != "") {
                $compatibles->compatibles = $request->request->get('pasteCompatibles');
            }
            if ($request->request->get('search_url') != "") {
                $compatibles->search_url = $request->request->get('search_url');
            }
            if ($request->request->get('apn1') != "") {
                $compatibles->apn1 = $request->request->get('apn1');
            }
            if ($request->request->get('apn2') != "") {
                $compatibles->apn2 = $request->request->get('apn2');
            }
            $compatibles->save();
        }

        if ($compatibles == "") {
            return response()->json([
                'success' => 500
            ]);
        } else {
            return response()->json([
                'success' => 200,
                'compatiblesID' => $compatibles->id
            ]);
        }
    }

    public function processTableAction($id, $compatiblesID, Request $request)
    {
        $model = $request->request->get('model');
        $product = Products::find($id);
        $query = ProductCompatiblesTable::locateModels($product, $compatiblesID, $model);
        $found = 0;
        foreach ($query as $q) {
            $found = 1;
        }
        if ($found == "0") {
            // new
            $data = new ProductCompatiblesTable();
            $data->product_id = $id;
            $data->compatibles_id = $compatiblesID;
            $data->apn = $model;
            $data->status = "review";
            $data->date_added = new \DateTime();
            $data->date_updated = new \DateTime();
            $data->save();

            return response()->json([
                'success' => 200
            ]);
        } else {
            // duplicate
            return response()->json([
                'success' => 500
            ]);
        }
    }

    public function updateSingleTableItem($recordID, Request $request)
    {
       $data = ProductCompatiblesTable::find($recordID);
       $data->apn = $request->request->get('model');
       $data->save();

        return response()->json([
            'success' => 200
        ]);
    }

    public function updateSingleTableItemStatus($recordID, Request $request)
    {
        $data = ProductCompatiblesTable::find($recordID);
        $data->status = $request->request->get('status'); // review or approved
        $data->save();

        return response()->json([
            'success' => 200
        ]);
    }

    public function deleteSingleTableItem($recordID)
    {
        $data = ProductCompatiblesTable::find($recordID);
        $compatableID = $data->compatibles_id;

        // delete manuals
        ProductCompatiblesManuals::deleteAllManuals($compatableID);

        // delete item
        $data->delete();

        return response()->json([
            'success' => 200
        ]);
    }

    public function getTableData($id, $compatiblesID)
    {
        $product = Products::find($id);
        $data = ProductCompatiblesTable::getTableData($product, $compatiblesID);
        return json_encode($data);
    }

    public function listManualsAction($id, $compatiblesID)
    {
        $page_title = "Compatibles :: Manuals";

        $product = Products::find($id);
        $data = ProductCompatiblesManuals::listManuals($product, $compatiblesID);
        $types = CompatiblesManualTypes::getManualTypes();

        return view('pages.compatibles.manuals', [
            'page_title' => $page_title,
            'id' => $id,
            'compatiblesID' => $compatiblesID,
            'data' => $data,
            'product' => $product,
            'types' => $types,
        ]);
    }

    public function saveManualsAction(Request $request)
    {
        $product_id = $request->request->get('id');
        $compatiblesID = $request->request->get('compatiblesID');
        $link = $request->request->get('link');
        $note = $request->request->get('note');
        $manual_id = $request->request->get('manual_id');

        $manual = new ProductCompatiblesManuals();
        $manual->product_id = $product_id;
        $manual->compatibles_id = $compatiblesID;
        $manual->link = $link;
        $manual->note = $note;
        $manual->manual_id = $manual_id;
        $manual->date_added = new \DateTime();
        $manual->date_updated = new \DateTime();
        $manual->save();

        $product = Products::find($product_id);
        $data = json_encode(ProductCompatiblesManuals::listManuals($product, $compatiblesID));

        return response()->json([
            'success' => 200,
            'data' => $data,
        ]);
    }

    public function deleteManualAction($recordID)
    {
        $data = ProductCompatiblesManuals::find($recordID);
        $data->delete();

        return response()->json([
            'success' => 200
        ]);
    }

    public function generateProductsAction($id)
    {
        $product = Products::find($id);

        $query = ProductCompatibles::locateCompatibles($product);
        $compatiblesID = "";

        foreach ($query as $q) {
            if (is_object($q)) {
                $compatibles = ProductCompatibles::find($q->id);
                $compatiblesID = $q->id;
            }
        }

        // get the list of compatibles
        $data = ProductCompatiblesTable::getTableData($product, $compatiblesID);
        $check = Products::checkChildDuplicates($id);
        $table = CompatiblesProductChildTable::countChild($id);
        $next_sku_counter = $table->count();

        if (is_object($data)) {
            foreach ($data as $d) {
                if ($d->status == "approved") {
                    // check for duplicates

                    if (is_object($check)) {
                        foreach ($check as $c) {
                            $duplicate = "0";
                            if (!is_null($c->primary_product_id)) {
                                // duplicate
                                $duplicate = "1";
                            }

                            if ($duplicate == "0") {
                                // clone product
                                $next_sku_counter = $next_sku_counter + 1;
                                $new_product = new Products();
                                $new_product->product_id = $product->product_id;
                                $new_product->name = $product->name;
                                $new_product->type = $product->type;
                                $new_product->sku = $product->sku . "-" . $next_sku_counter;
                                $new_product->mpn = $d->apn; // new model
                                $new_product->description = $product->description;
                                $new_product->weight = $product->weight;
                                $new_product->width = $product->width;
                                $new_product->depth = $product->depth;
                                $new_product->height = $product->height;
                                $new_product->price = $product->price;
                                $new_product->cost_price = $product->cost_price;
                                $new_product->retail_price = $product->retail_price;
                                $new_product->sale_price = $product->sale_price;
                                $new_product->tax_class_id = $product->tax_class_id;
                                $new_product->product_tax_code = $product->product_tax_code;
                                $new_product->brand_id = $product->brand_id;
                                $new_product->brand_name = $product->brand_name;
                                $new_product->inventory_level = $product->inventory_level;
                                $new_product->inventory_level_new = $product->inventory_level_new;
                                $new_product->inventory_level_used = $product->inventory_level_used;
                                $new_product->inventory_warning_level = $product->inventory_warning_level;
                                $new_product->inventory_tracking = $product->inventory_tracking;
                                $new_product->fixed_cost_shipping_price = $product->fixed_cost_shipping_price;
                                $new_product->is_free_shipping = $product->is_free_shipping;
                                $new_product->is_visible = $product->is_visible;
                                $new_product->is_featured = $product->is_featured;
                                $new_product->warranty = $product->warranty;
                                $new_product->bin_picking_number = $product->bin_picking_number;
                                $new_product->layout_file = $product->layout_file;
                                $new_product->upc = $product->upc;
                                $new_product->search_keywords = $product->search_keywords;
                                $new_product->availability = $product->availability;
                                $new_product->availability_description = $product->availability_description;
                                $new_product->gift_wrapping_options_type = $product->gift_wrapping_options_type;
                                $new_product->sort_order = $product->sort_order;
                                $new_product->product_condition = $product->product_condition;
                                $new_product->is_condition_shown = $product->is_condition_shown;
                                $new_product->order_quantity_minimum = $product->order_quantity_minimum;
                                $new_product->order_quantity_maximum = $product->order_quantity_maximum;
                                $new_product->page_title = $product->page_title;
                                $new_product->meta_description = $product->meta_description;
                                $new_product->meta_keywords = $product->meta_keywords;
                                $new_product->view_count = $product->view_count;
                                $new_product->preorder_release_date = $product->preorder_release_date;
                                $new_product->preorder_message = $product->preorder_message;
                                $new_product->is_preorder_only = $product->is_preorder_only;
                                $new_product->is_price_hidden = $product->is_price_hidden;
                                $new_product->price_hidden_label = $product->price_hidden_label;
                                $new_product->calculated_price = $product->calculated_price;
                                $new_product->date_created = $product->date_created;
                                $new_product->date_modified = $product->date_modified;
                                $new_product->image_age = $product->image_age;
                                $new_product->variant_id = $product->variant_id;
                                $new_product->adwords_on = $product->adwords_on;
                                $new_product->google_shopping_on = $product->google_shopping_on;
                                $new_product->google_shopping_exclude = $product->google_shopping_exclude;
                                $new_product->is_compatible = $product->is_compatible;
                                $new_product->status = $product->status;
                                $new_product->last_inventory = $product->last_inventory;
                                $new_product->compatible_status = $product->compatible_status;
                                $new_product->adwords_status = $product->adwords_status;
                                $new_product->ebay_eligible = $product->ebay_eligible;
                                $new_product->amazon_eligible = $product->amazon_eligible;
                                $new_product->amazon_flag_pricing = $product->amazon_flag_pricing;
                                $new_product->amazon_flag_no_asin = $product->amazon_flag_no_asin;
                                $new_product->amazon_flag_restricted = $product->amazon_flag_restricted;
                                $new_product->amazon_flag_needs_support_call = $product->amazon_flag_needs_support_call;
                                $new_product->amazon_flag_other = $product->amazon_flag_other;
                                $new_product->amazon_flag_watch_ranking = $product->amazon_flag_watch_ranking;
                                $new_product->marketability_score = $product->marketability_score;
                                $new_product->google_eligible = $product->google_eligible;
                                $new_product->is_watched = $product->is_watched;
                                $new_product->is_purchased = $product->is_purchased;
                                $new_product->is_data_error = $product->is_data_error;
                                $new_product->last_updated = $product->last_updated;
                                $new_product->processing_flag = $product->processing_flag;
                                $new_product->last_optimized = $product->last_optimized;
                                $new_product->product_marketable_status = $product->product_marketable_status;
                                $new_product->last_date_have_inventory = $product->last_date_have_inventory;
                                $new_product->last_date_sold_out = $product->last_date_sold_out;
                                $new_product->is_prime = $product->is_prime;
                                $new_product->battery_cover = $product->battery_cover;
                                $new_product->classification = $product->classification;
                                $new_product->device = $product->device;
                                $new_product->bluetooth = $product->bluetooth;
                                $new_product->wifi = $product->wifi;
                                $new_product->rf = $product->rf;
                                $new_product->infrared = $product->infrared;
                                $new_product->star_rating = $product->star_rating;
                                $new_product->save();

                                // add to table
                                $new_compatible = new CompatiblesProductChildTable();
                                $new_compatible->primary_product_id = $id;
                                $new_compatible->child_product_id = $new_product->id;
                                $new_compatible->date_added = new \DateTime();
                                $new_compatible->date_updated = new \DateTime();
                                $new_compatible->save();

                            }

                        }
                    }
                }
            }

        }
        return redirect()->route('products.compatibles.list', $id)->with('message', "If any product qualified to be cloned it has completed.");
    }
}
