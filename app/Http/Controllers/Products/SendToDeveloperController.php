<?php

namespace App\Http\Controllers\Products;

use App\Models\Flags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SearchLinksCategories;
use App\Models\DeveloperQueue;
use App\Models\Products;

class SendToDeveloperController extends Controller
{
    public function addAction($id, Request $request)
    {
        $product = Products::find($id);

        /*
         * Create a model view and pass $product->id to the view.
         *
         * In your view pass the following to save:
         * id: (hidden) $product->id
         * bug: (checkbox) value on or off
         * new_feature: (checkbox) value on or off
         * question: (checkbox) value on or off
         * other: (checkbox) value on or off
         * comments: (textarea)
         */

        return response()->json([
            'id' => $product->id,
        ]);
    }

    public function saveAction(Request $request)
    {
        $queue = new DeveloperQueue();

        /**
         * Note: I like to use the following that comes from Symfony:
         * $request->request->get('field') : This is for POST fields
         * $request->query->get('field') : This is for GET fields.
         * Please follow this in Laravel as Laravel might have its own way
         * but I prefer to follow the Symfony way.
         */

        $queue->product_id = $request->request->get('id');
        if ($request->request->get('bug') != "") {
            $bug = "on";
        } else {
            $bug = "off";
        }
        $queue->bug = $bug;

        if ($request->request->get('new_feature') != "") {
            $new_feature = "on";
        } else {
            $new_feature = "off";
        }
        $queue->new_feature = $new_feature;

        if ($request->request->get('question') != "") {
            $question = "on";
        } else {
            $question = "off";
        }
        $queue->question = $question;

        if ($request->request->get('other') != "") {
            $other = "on";
        } else {
            $other = "off";
        }
        $queue->other = $other;

        $queue->comments = $request->request->get('comments');

        $user = $request->user();
        $queue->userID = $user->id;

        $queue->status = "open";
        $queue->date_added = new \DateTime();
        $queue->date_updated = new \DateTime();

        $queue->save();

        return response()->json([
            'success' => 200,
            'message' => "The queue was saved."
        ]);
    }
}
