<?php

namespace App\Http\Controllers\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\AdminManageFlags;
use App\Models\Flags;

class FlagsController extends Controller
{
    public function listAction($id, Request $request)
    {
        $page_title = "Flags";
        $product = Products::find($id);
        $flags = AdminManageFlags::All();

        $data = Flags::getFlags($product);

        return view('pages.flags.list', [
            'id' => $id,
            'page_title' => $page_title,
            'product' => $product,
            'flags' => $flags,
            'data' => $data,
        ]);
    }
    public function listForProduct($id)
    {
        $flags = Flags::productFlags($id);

        return response()->json([
            'success' => 200,
            'active_flags' => $flags,
        ]);
    }

    public function saveAction(Request $request, $id)
    {
        $flag = new Flags();
        $flag->product_id = $id;
        $flag->admin_flag_id = $request->flag;

        $flag->date_added = new \DateTime();
        $flag->date_updated = new \DateTime();
        $flag->save(); // ================   added ===================

        return response()->json([
            'success' => 200,
            'message' => "The flag was saved."
        ]);
    }

    public function deleteAction($id, $flag, Request $request)
    {
        Flags::deleteFlag($flag, $id);

        return response()->json([
            'success' => 200,
            'message' => "The flag was removed."
        ]);
    }
}
