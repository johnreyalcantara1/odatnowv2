<?php

namespace App\Http\Controllers\Products;

use App\Models\Alarms;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\MpnMarketStatus;
use App\Models\DeviceType;
use App\Http\Controllers\Products\NotificationController;

use function GuzzleHttp\Promise\all;

class OptimizationController extends Controller
{
    public function listAction(Request $request)
    {
        $page_title = "Products";
        $search_text = $request->query->get('search_text');
        $data = Products::productList($search_text);

        return view('pages.products.list', [
            'page_title' => $page_title,
            'data' => $data,
            'search_text' => $search_text,
        ]);
    }

    public function optimizationAction($id)
    {
        $page_title = "Optimization";
        $device = DeviceType::All();
        $data = Products::optimization($id);

        $market = MpnMarketStatus::mpnStatus($data, $id);
        if (is_null($market)) {
            // fall back original design
            $market = MpnMarketStatus::where('mpn', $data[0]->mpn)->first();
        }
        if (is_null($market)) {
            $market = array();
        }

        $getNext = json_decode(Products::getNextForReview($id), true);
        $getPrior = json_decode(Products::getPriorForReview($id), true);
        $nextID = "0";
        $priorID = "0";

        if (is_array($getNext)) {
            if (!empty($getNext)) {
                $nextID = $getNext[0]['id'];
            }
        }

        if (is_array($getPrior)) {
            if (!empty($getPrior)) {
                $priorID = $getPrior[0]['id'];
            }
        }

        $notifications = NotificationController::getActiveAlerts($id);
        $alarms = Alarms::alarms($id);

        return view('pages.products.optimization', [
            'page_title' => $page_title,
            'data' => $data,
            'market' => $market,
            'device' => $device,
            'priorID' => $priorID,
            'nextID' => $nextID,
            'notifications' => $notifications,
            'alarms' => $alarms,
        ]);
    }

    public function ajaxToggleClassificationAction(Request $request)
    {
        $product = Products::find($request->id);
        $product->classification = $request->data;
        $product->save();

        return response()->json([
            'success' => 200,
            'product' => $product,
        ]);
    }

    public function ajaxToogleMarketStatusAction(Request $request)
    {
        $mpn = $request->mpn;
        $id = $request->id;
        $status = $request->data;

        $product = Products::find($request->request->get('id'));

        // Find by mpn and
        $market = MpnMarketStatus::where('mpn', $mpn)->where('productID', $id)->first();
        if (is_null($market)) {
            // fall back original design
            $market = MpnMarketStatus::where('mpn', $mpn)->first();
        }
        if (is_null($market)) {
            // Insert new
            $market = new MpnMarketStatus();
            $market->mpn = $mpn;
            $market->status = $status;
            $market->productID = $id;
            $market->save();
        } else {
            $market->status = $status;
            $market->save();
        }

        $m = MpnMarketStatus::where('mpn', $product->mpn)->where('productID', $id)->first();
        if (is_null($m)) {
            // fall back original design
            $m = MpnMarketStatus::where('mpn', $product->mpn)->first();
        }
        if (is_null($m)) {
            $m = array();
        }

        return response()->json([
            'success' => 200,
            'product' => $product,
            'market' => $m
        ]);
    }

    public function ajaxDeviceTypeAction(Request $request)
    {
        $id = $request->id;
        $device = $request->data;
        $product = Products::find($id);
        $product->device = $device;
        $product->save();

        return null;
    }

    public function ajaxCompleteAction($id, $status, Request $request)
    {
        $product = Products::find($id);
        if ($status == "complete") {
            $product->status = "complete";
        } else {
            $product->status = "review";
        }
        $product->save();

        return response()->json([
            'success' => 200,
            'status' => $product->status
        ]);
    }

    public function listSettings()
    {
        return view('pages.products.settings.list');
    }
}
