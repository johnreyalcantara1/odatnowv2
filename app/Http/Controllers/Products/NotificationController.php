<?php

namespace App\Http\Controllers\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\Notifications;

class NotificationController extends Controller
{
    public function listAction($id, Request $request)
    {
        $page_title = "Notifications";

        $product = Products::find($id);

        $data = Notifications::notifications($product);

        return view('pages.notifications.list', [
            'id' => $id,
            'page_title' => $page_title,
            'data' => $data,
            'product' => $product,
        ]);
    }

    public function addAction($id, Request $request)
    {
        $page_title = "Add Notification";

        $product = Products::find($id);

        return view('pages.notifications.add', [
            'id' => $id,
            'product' => $product,
            'page_title' => $page_title,
        ]);
    }

    public function saveAction(Request $request)
    {
        $id = $request->request->get('id');
        $user = $request->user();

        $product = Products::find($id);

        $dismiss = $request->request->get('dismiss');
        if ($dismiss == "") {
            $dismiss = "off";
        }

        $snooze = $request->request->get('snooze');
        if ($snooze == "") {
            $snooze = "off";
        }

        $notification = new Notifications();
        $notification->product_id = $product->product_id;
        $notification->sku = $product->sku;
        $notification->user = $user->id;
        $notification->reason = $request->request->get('reason');
        $notification->notes = $request->request->get('notes');
        $notification->dismiss = $dismiss;
        $notification->snooze = $snooze;
        $notification->date_added = new \DateTime();
        $notification->date_updated = new \DateTime();
        $notification->save();

        return redirect()->route('products.notifications.list', $id)->with('message', "The notification was added.");
    }

    public function dismissAction($id, Request $request)
    {
        $notification = Notifications::find($id);
        if ($request->request->get('dismiss') == "on") {
            $notification->dismiss = "on";
        } else {
            $notification->dismiss = "off";
        }
        $notification->save();
        return($request->request->get('dismiss'));
    }

    public function snoozeAction($id, Request $request)
    {
        $notification = Notifications::find($id);
        if ($request->request->get('snooze') == "on") {
            $notification->snooze = "on";
        } else {
            $notification->snooze = "off";
        }
        $notification->save();
        return($request->request->get('snooze'));
    }

    public static function getActiveAlerts($id)
    {
        $product = Products::find($id);
        $count = Notifications::alerts($product);
        return($count);
    }
}
