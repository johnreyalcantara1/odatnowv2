<?php

namespace App\Http\Controllers\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\ProductOrders;
use App\Models\ProductsOptimization;
use App\Models\ProductLogs;
use App\Models\Categories;
use App\Services\BigCommerceApi;

class DeleteController extends Controller
{
    public function deleteAction($id, Request $request, BigCommerceApi $bc)
    {
        /**
         * The logged in user role check : not the user to edit
         */
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }
        /**
         * end security
         */

        $product = Products::find($id);

        if ($product->inventory_level > "0") {
            return redirect()->route('products.optimization')->with('error', "The product still has inventory that must be removed first. Delete failed.");
        }

        // Disable for testing
        $product->status = "trash";
        $product->save();

        // Do API calls : TBD
        $result = $bc->deleteProduct($id);

        if ($result == "204") {
            return redirect()->route('products.list')->with('message', "The product was deleted.");
        } else {
            return redirect()->route('products.list')->with('error', "The product was removed but failed to delete on Big Commerce.");
        }

    }
}
