<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;
use phpseclib\Crypt\Random;

class AdminBrandsController extends Controller
{
    public function listAction(Request $request)
    {
        $page_title = 'Brands';
        $search = $request->search;
        $brands = Brand::where( function($query)use ($search) {
            if ($search != "") {
                $query->where('name', 'like', '%'.$search.'%');
            }
        })->orderBy('id', 'DESC')->paginate($_ENV['PAGINATE']);
        return view('pages.admin.brands.list',
            [
                'brands' => $brands,
                'page_title' => $page_title,
            ]);
    }

    public function newAction()
    {
        $page_title = 'Create new brand';

        return view('pages.admin.brands.new',[
            'page_title' => $page_title
        ]);
    }

    public function saveAction(Request $request)
    {
        $validatedData = $request->validate([
            'brand_id' => 'required|numeric|unique:brand,brand_id',
            'name' => 'required|string',
        ]);
        $brand = new Brand();
        $brand->brand_id = $request->brand_id;
        $brand->name = $request->name;
        $brand->save();

        return redirect()->route('admin.brands.list')->with('message', 'Brand was successfully created');
    }

    public function editAction($id)
    {
        $brand = Brand::find($id);
        $page_title = 'Edit Brand';
        return view('pages.admin.brands.edit',
            [
                'page_title' => $page_title,
                'brand' => $brand
            ]);
    }
    public function updateAction(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string',
        ]);
        $brand = Brand::find($request->id);
        if ($brand->name == $request->name){
            return redirect()->back()->with('message', "No changes has been made");
        }
        $brand->name = $request->name;
        $brand->save();
        //  brands credentials
        return redirect()->route('admin.brands.list')->with('message', 'Brand was successfully updated');
    }
}
