<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use Illuminate\Support\Facades\DB;

class AdminUsersController extends Controller
{
    public function listAction(Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $data = User::orderBy('name')->paginate($_ENV['PAGINATE']);
        $roles = Role::All();
        $attachedRoles = array();

        foreach ($data as $d) {
            $u = User::find($d->id);
            foreach ($roles as $role){
                if ($u->hasRole($role->slug) == true) {
                    $attachedRoles[$d->id][] = $role->name;
                }
            }
        }

        return view('pages.admin.users.list', [
            'data' => $data,
            'attachedRoles' => $attachedRoles,
            'currentUser' => $user,
        ]);
    }

    public function newAction(Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $roles = Role::All();
        return view('pages.admin.users.new', [
            'roles' => $roles,
        ]);
    }

    public function saveAction(Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        // check for duplicate
        $users = User::where('email', '=', $request->request->get('email'))->get();
        $found = "0";
        foreach ($users as $user) {
            $found = "1";
        }

        if ($found == "1") {
            return redirect()->route('admin.users.new')->with('message', "The email {$request->request->get('email')} is already registered with another user.");
        }

        // verify form
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password1' => 'required',
            'password2' => 'required',
            'roles' => 'required'
        ]);

        // Verify the passwords
        if ($request->request->get('password1') != $request->request->get('password2')) {
            return redirect()->route('admin.users.new')->with('message', "The password entered did not match.");
        }
        if ($request->request->get('password1') == "") {
            return redirect()->route('admin.users.new')->with('message', "The password was empty.");
        }

        // encrypt password
        $roles = $request->request->get('roles');
        if (empty($roles)) {
            return redirect()->route('admin.users.new')->with('message', "You did not select a security group.");
        }

        // save user
        $user = new User();
        $user->name = $request->request->get('name');
        $user->email = $request->request->get('email');
        $user->password = bcrypt($request->request->get('password1'));
        $user->save();

        // assign user roles
        foreach ($roles as $key => $value) {
            $user_role = Role::where('slug', $value)->first();
            $user->roles()->attach($user_role);
        }

        return redirect()->route('admin.users.list')->with('message', "The new user was created.");
    }

    public function showAction($id, Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $data = User::find($id);
        $roles = Role::All();
        $attachedRoles = array();

        foreach ($roles as $role){
            if ($user->hasRole($role->slug) == true) {
                $attachedRoles[] = $role->name;
            }
        }

        return view('pages.admin.users.show', [
            'data' => $data,
            'attachedRoles' => $attachedRoles,
        ]);
    }

    public function editAction($id, Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $data = User::find($id);
        $roles = Role::All();
        $attachedRoles = array();

        foreach ($roles as $role){
            if ($user->hasRole($role->slug) == true) {
                $attachedRoles[] = $role->name;
            }
        }

        return view('pages.admin.users.edit', [
            'id' => $id,
            'data' => $data,
            'roles' => $roles,
            'attachedRoles' => $attachedRoles,
        ]);
    }

    public function updateAction(Request $request)
    {
        /**
         * The logged in user role check : not the user to edit
         */
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }
        /**
         * end security
         */

        $id = $request->request->get('id');

        // check for duplicate
        $users = User::where('email', '=', $request->request->get('email'))->get();
        $found = "0";
        foreach ($users as $user) {
            $found++;
        }

        if ($found > "1") {
            return redirect()->route('admin.users.edit', ['id' => $id])->with('message', "The email {$request->request->get('email')} is already registered with another user.");
        }

        $roles = $request->request->get('roles');
        if (empty($roles)) {
            return redirect()->route('admin.users.edit', ['id' => $id])->with('message', "You did not select a security group.");
        }

        // save user
        $user = User::find($id);
        $user->name = $request->request->get('name');
        $user->email = $request->request->get('email');
        if ($request->request->get('password') != "") {
            $user->password = bcrypt($request->request->get('password'));
        }
        $user->save();

        // remove all existing roles
        DB::table('users_roles')->where('users_roles.user_id', '=', $id)->delete();

        // add roles
        foreach ($roles as $key => $value) {
            $user_role = Role::where('slug', $value)->first();
            $user->roles()->attach($user_role);
        }

        return redirect()->route('admin.users.list')->with('message', "The user was updated.");
    }

    public function deleteAction($id, Request $request)
    {
        /**
         * The logged in user role check : not the user to edit
         */
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }
        /**
         * end security
         */

        if ($user->id == $id) {
            return redirect()->route('admin.users.list')->with('message', "Error Code: ID10T : You can not delete yourself.");
        }

        // remove all existing roles
        DB::table('users_roles')->where('users_roles.user_id', '=', $id)->delete();
        DB::table('users_permissions')->where('users_permissions.user_id', '=', $id)->delete();

        $u = User::find($id);
        $u->delete($u->id);

        return redirect()->route('admin.users.list')->with('message', "The user was removed.");
    }
}
