<?php

use Illuminate\Support\Facades\Route;

/**
 * Routes :: Admin :: Users
 */
Route::get('/admin/users', 'Admin\AdminUsersController@listAction')->name('admin.users.list')->middleware('auth');
Route::get('/admin/users/new', 'Admin\AdminUsersController@newAction')->name('admin.users.new')->middleware('auth');
Route::get('/admin/users/{id}', 'Admin\AdminUsersController@showAction')->name('admin.users.show')->middleware('auth');
Route::get('/admin/users/edit/{id}', 'Admin\AdminUsersController@editAction')->name('admin.users.edit')->middleware('auth');
Route::get('/admin/users/delete/{id}', 'Admin\AdminUsersController@deleteAction')->name('admin.users.delete')->middleware('auth');
Route::post('/admin/users/save', 'Admin\AdminUsersController@saveAction')->name('admin.users.save')->middleware('auth');
Route::post('/admin/users/update', 'Admin\AdminUsersController@updateAction')->name('admin.users.update')->middleware('auth');

/**
 * Routes :: Admin :: Roles
 */
Route::get('/admin/roles', 'Admin\AdminRolesController@listAction')->name('admin.roles.list')->middleware('auth');
Route::get('/admin/roles/new', 'Admin\AdminRolesController@newAction')->name('admin.roles.new')->middleware('auth');
Route::get('/admin/roles/{id}', 'Admin\AdminRolesController@showAction')->name('admin.roles.show')->middleware('auth');
Route::get('/admin/roles/edit/{id}', 'Admin\AdminRolesController@editAction')->name('admin.roles.edit')->middleware('auth');
Route::post('/admin/roles/save', 'Admin\AdminRolesController@saveAction')->name('admin.roles.save')->middleware('auth');
Route::post('/admin/roles/update', 'Admin\AdminRolesController@updateAction')->name('admin.roles.update')->middleware('auth');

/**
 * Routes :: Admin :: Device Types
 */
Route::get('/admin/devicetype', 'Admin\AdminDeviceTypeController@listAction')->name('admin.devicetype.list')->middleware('auth');
Route::get('/admin/devicetype/new', 'Admin\AdminDeviceTypeController@newAction')->name('admin.devicetype.new')->middleware('auth');
Route::get('/admin/devicetype/edit/{id}', 'Admin\AdminDeviceTypeController@editAction')->name('admin.devicetype.edit')->middleware('auth');
Route::post('/admin/devicetype/save', 'Admin\AdminDeviceTypeController@saveAction')->name('admin.devicetype.save')->middleware('auth');
Route::post('/admin/devicetype/update', 'Admin\AdminDeviceTypeController@updateAction')->name('admin.devicetype.update')->middleware('auth');

/**
 * Routes :: Admin :: Search Links Categories
 */
Route::get('/admin/searchlinkscategories', 'Admin\AdminSearchLinksCategoriesController@listAction')->name('admin.searchlinkscategories.list')->middleware('auth');
Route::get('/admin/searchlinkscategories/new', 'Admin\AdminSearchLinksCategoriesController@newAction')->name('admin.searchlinkscategories.new')->middleware('auth');
Route::get('/admin/searchlinkscategories/edit/{id}', 'Admin\AdminSearchLinksCategoriesController@editAction')->name('admin.searchlinkscategories.edit')->middleware('auth');
Route::post('/admin/searchlinkscategories/save', 'Admin\AdminSearchLinksCategoriesController@saveAction')->name('admin.searchlinkscategories.save')->middleware('auth');
Route::post('/admin/searchlinkscategories/update', 'Admin\AdminSearchLinksCategoriesController@updateAction')->name('admin.searchlinkscategories.update')->middleware('auth');


/**
 * Routes :: Admin :: Manage Flags
 */
Route::get('/admin/flags', 'Admin\AdminFlagsController@listAction')->name('admin.flags.list')->middleware('auth');
Route::get('/admin/flags/new', 'Admin\AdminFlagsController@newAction')->name('admin.flags.new')->middleware('auth');
Route::get('/admin/flags/edit/{id}', 'Admin\AdminFlagsController@editAction')->name('admin.flags.edit')->middleware('auth');
Route::post('/admin/flags/save', 'Admin\AdminFlagsController@saveAction')->name('admin.flags.save')->middleware('auth');
Route::post('/admin/flags/update', 'Admin\AdminFlagsController@updateAction')->name('admin.flags.update')->middleware('auth');

/**
 * Routes :: Admin :: Developer Queue
 */
Route::get('/admin/developerqueue', 'Admin\AdminDeveloperQueueController@listAction')->name('admin.developerqueue.list')->middleware('auth');
Route::get('/admin/developerqueue/edit/{id}', 'Admin\AdminDeveloperQueueController@editAction')->name('admin.developerqueue.edit')->middleware('auth');
Route::post('/admin/developerqueue/update', 'Admin\AdminDeveloperQueueController@updateAction')->name('admin.developerqueue.update')->middleware('auth');
Route::post('/admin/developerqueue/update', 'Admin\AdminDeveloperQueueController@updateAction')->name('admin.developerqueue.update')->middleware('auth');

/**
 * Routes :: Admin :: Store
 */

Route::get('/admin/stores', 'Admin\AdminStoresController@listAction')->name('admin.stores.list');
Route::get('/admin/stores/new', 'Admin\AdminStoresController@newAction')->name('admin.stores.new');
Route::get('/admin/stores/edit/{id}', 'Admin\AdminStoresController@editAction')->name('admin.stores.edit');
Route::post('/admin/stores/save', 'Admin\AdminStoresController@saveAction')->name('admin.stores.save');
Route::post('/admin/stores/update', 'Admin\AdminStoresController@updateAction')->name('admin.stores.update');
/**
 * Routes :: Admin :: Brands
 */

Route::get('/admin/brands', 'Admin\AdminBrandsController@listAction')->name('admin.brands.list');
Route::get('/admin/brands/new', 'Admin\AdminBrandsController@newAction')->name('admin.brands.new');
Route::get('/admin/brands/edit/{id}', 'Admin\AdminBrandsController@editAction')->name('admin.brands.edit');
Route::post('/admin/brands/save', 'Admin\AdminBrandsController@saveAction')->name('admin.brands.save');
Route::post('/admin/brands/update', 'Admin\AdminBrandsController@updateAction')->name('admin.brands.update');

