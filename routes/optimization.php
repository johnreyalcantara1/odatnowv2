<?php

use Illuminate\Support\Facades\Route;

// ODAT Optimization
Route::post('/products/optimization/ajax/classification', 'Products\OptimizationController@ajaxToggleClassificationAction')->name('products.ajax.classification')->middleware('auth');
Route::post('/products/optimization/ajax/market', 'Products\OptimizationController@ajaxToogleMarketStatusAction')->name('products.ajax.market')->middleware('auth');
Route::post('/products/optimization/ajax/device', 'Products\OptimizationController@ajaxDeviceTypeAction')->name('products.ajax.device')->middleware('auth');
Route::get('/products/optimization/ajax/complete/{id}/{status}', 'Products\OptimizationController@ajaxCompleteAction')->name('products.ajax.complete')->middleware('auth');
Route::get('/products/optimization/notifications/{id}', 'Products\NotificationController@listAction')->name('products.notifications.list')->middleware('auth');
Route::get('/products/optimization/notifications/add/{id}', 'Products\NotificationController@addAction')->name('products.notifications.add')->middleware('auth');
Route::post('/products/optimization/notifications/save', 'Products\NotificationController@saveAction')->name('products.notifications.save')->middleware('auth');
Route::post('/products/optimization/notifications/dismiss/{id}', 'Products\NotificationController@dismissAction')->name('products.notifications.dismiss')->middleware('auth');
Route::post('/products/optimization/notifications/snooze/{id}', 'Products\NotificationController@snoozeAction')->name('products.notifications.snooze')->middleware('auth');
Route::get('/products/optimization/flags/{id}', 'Products\FlagsController@listAction')->name('products.flags.list')->middleware('auth');
Route::get('/products/optimization/flagsForProduct/{id}', 'Products\FlagsController@listForProduct')->name('products.flags.listForProduct')->middleware('auth');
Route::post('/products/optimization/flags/save/{id}', 'Products\FlagsController@saveAction')->name('products.flags.save')->middleware('auth');
Route::get('/products/optimization/flags/delete/{id}/{flag}', 'Products\FlagsController@deleteAction')->name('products.flags.delete')->middleware('auth');
Route::get('/products/optimization/alarm/{id}', 'Products\AlarmController@editAction')->name('products.alarm.edit')->middleware('auth');
Route::post('/products/optimization/alarm/save', 'Products\AlarmController@saveAction')->name('products.alarm.save')->middleware('auth');
Route::get('/products/optimization/wireless/bluetooth/{id}', 'Products\WirelessController@toggleBluetoothAction')->name('products.wireless.bluetooth')->middleware('auth');
Route::get('/products/optimization/wireless/wifi/{id}', 'Products\WirelessController@toggleWifiAction')->name('products.wireless.wifi')->middleware('auth');
Route::get('/products/optimization/wireless/rf/{id}', 'Products\WirelessController@toggleRfAction')->name('products.wireless.rf')->middleware('auth');
Route::get('/products/optimization/wireless/infrared/{id}', 'Products\WirelessController@toggleInfraRedAction')->name('products.wireless.infrared')->middleware('auth');
Route::get('/products/optimization/starrating/{id}/{star}', 'Products\StarRatingController@toggleStarAction')->name('products.starrating')->middleware('auth');

Route::post('/products/optimization/location/{id}', 'Products\LocationController@updateLocationAction')->name('products.location.update')->middleware('auth');
Route::post('/products/optimization/bin/{id}', 'Products\LocationController@updateBinAction')->name('products.bin.update')->middleware('auth');
Route::post('/products/optimization/qoh/{id}', 'Products\LocationController@updateQohAction')->name('products.qoh.update')->middleware('auth');

Route::get('/products/optimization/searchlinks/{id}', 'Products\SearchLinksController@mainListAction')->name('products.searchlinks.main')->middleware('auth');
Route::get('/products/optimization/searchlinks/category/{id}/{categoryID}', 'Products\SearchLinksController@listAction')->name('products.searchlinks.list')->middleware('auth');
Route::get('/products/optimization/searchlinks/add/{id}/{categoryID}', 'Products\SearchLinksController@addAction')->name('products.searchlinks.add')->middleware('auth');
Route::post('/products/optimization/searchlinks/save', 'Products\SearchLinksController@saveAction')->name('products.searchlinks.save')->middleware('auth');
Route::get('/products/optimization/searchlinks/delete/{recordID}/{id}/{categoryID}', 'Products\SearchLinksController@deleteAction')->name('products.searchlinks.delete')->middleware('auth');
Route::get('/products/optimization/assignto/{id}', 'Products\AssignToController@assignToAction')->name('products.assignto.assign')->middleware('auth');
Route::post('/products/optimization/assignto/save', 'Products\AssignToController@saveAction')->name('products.assignto.save')->middleware('auth');
Route::post('/products/optimization/sendto/save', 'Products\SendToDeveloperController@saveAction')->name('products.sendto.save')->middleware('auth');

// Amazon Details
Route::get('/products/optimization/amazon/{id}', 'Products\AmazonDetailsController@detailsAction')->name('products.amazon.details')->middleware('auth');

// Compatibles
Route::get('/products/optimization/compatibles/list/{id}', 'Products\CompatiblesController@listAction')->name('products.compatibles.list')->middleware('auth');
Route::post('/products/optimization/compatibles/save', 'Products\CompatiblesController@saveAction')->name('products.compatibles.save')->middleware('auth');
Route::post('/products/optimization/compatibles/process/{id}/{compatiblesID}', 'Products\CompatiblesController@processTableAction')->name('products.compatibles.process')->middleware('auth');
Route::get('/products/optimization/compatibles/data/{id}/{compatiblesID}', 'Products\CompatiblesController@getTableData')->name('products.compatibles.data')->middleware('auth');
Route::get('/products/optimization/compatibles/manuals/list/{id}/{compatiblesID}', 'Products\CompatiblesController@listManualsAction')->name('products.compatibles.manuals.list')->middleware('auth');
Route::post('/products/optimization/compatibles/manuals/save', 'Products\CompatiblesController@saveManualsAction')->name('products.compatibles.manuals.save')->middleware('auth');
Route::get('/products/optimization/compatibles/manuals/delete/{recordID}', 'Products\CompatiblesController@deleteManualAction')->name('products.compatibles.manuals.delete')->middleware('auth');
Route::post('/products/optimization/compatibles/update/{recordID}', 'Products\CompatiblesController@updateSingleTableItem')->name('products.compatibles.update')->middleware('auth');
Route::post('/products/optimization/compatibles/update/status/{recordID}', 'Products\CompatiblesController@updateSingleTableItemStatus')->name('products.compatibles.update.status')->middleware('auth');
Route::get('/products/optimization/compatibles/delete/{recordID}', 'Products\CompatiblesController@deleteSingleTableItem')->name('products.compatibles.delete')->middleware('auth');
Route::get('/products/optimization/compatibles/generate/{id}', 'Products\CompatiblesController@generateProductsAction')->name('products.compatibles.generate')->middleware('auth');


//analytics
Route::get('/products/optimization/analytics', 'Products\GoogleAnalyticsController@listAction')->name('products.analytics.list');

//settings
Route::get('/products/optimization/settings', 'Products\OptimizationController@listSettings')->name('products.settings.list');
