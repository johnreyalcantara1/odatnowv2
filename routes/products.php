<?php

use Illuminate\Support\Facades\Route;

// ODAT Routes :: Products
Route::get('/products/list', 'Products\OptimizationController@listAction')->name('products.list')->middleware('auth');
Route::post('/products/list', 'Products\OptimizationController@listAction')->name('products.search')->middleware('auth');
Route::get('/products/optimization/{id}', 'Products\OptimizationController@optimizationAction')->name('products.optimization')->middleware('auth');
Route::get('/products/delete/{id}', 'Products\DeleteController@deleteAction')->name('products.delete')->middleware('auth');

// Image Upload
Route::get('/products/image/test/{id}', 'Products\ImageEditController@testAction')->name('products.imageupload.test')->middleware('auth');
Route::post('/products/image/upload', 'Products\ImageEditController@uploadImageAction')->name('product.imageupload.save')->middleware('auth');
Route::get('/products/image/draft/{id}', 'Products\ImageEditController@getAllImagesAction')->name('products.imageupload.getall')->middleware('auth');
Route::get('/products/image/draft/singleimage/{id}', 'Products\ImageEditController@getSingleImageAction')->name('products.imageupload.singleimage')->middleware('auth');
Route::get('/products/image/draft/deleteimage/{id}', 'Products\ImageEditController@deleteSingleImageAction')->name('products.imageupload.deleteimage')->middleware('auth');
Route::get('/products/image/draft/changeimagetype/{id}/{type}', 'Products\ImageEditController@editSingleImageTypeAction')->name('products.imageupload.editimagetype')->middleware('auth');

// Process Image
Route::get('/products/image/process/{id}/{imageID}', 'Products\ImageProcessController@processImageAction')->name('products.imageprocess.process')->middleware('auth');

// New Product
Route::get('/products/new', 'Products\AddProductController@newAction')->name('products.new')->middleware('auth');
// Test Route :: To be deleted soon
Route::get('/products/test', 'Products\NewProductController@saveAction')->name('products.test')->middleware('auth');

