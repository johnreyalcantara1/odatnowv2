<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Logout
 */
Route::get('/logout', 'Auth\LogoutController@logoutAction')->name('logout');

/**
 * Dashboard
 */
Route::get('/', 'PagesController@index')->name('dashboard')->middleware('auth');

/**
 * Routes :: User :: Profile
 */
Route::get('/profile', 'User\ProfileController@showAction')->name('profile.show')->middleware('auth');
Route::get('/profile/edit', 'User\ProfileController@editAction')->name('profile.edit')->middleware('auth');
Route::post('/profile/update', 'User\ProfileController@updateAction')->name('profile.update')->middleware('auth');


// Quick search dummy route to display html elements in search dropdown (header search)
Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search')->middleware('auth');

// Disable public registration, all other auth are allowed.
Auth::routes(['register' => false]);


