<?php

use Illuminate\Support\Facades\Route;

Route::get("/bulk/list", "Bulk\BulkController@listAction")->name('bulk.list')->middleware('auth');
